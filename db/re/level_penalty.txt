// Experience & Drop Rate Modifier Database
//
// Structure of Database:
// Type,Race,Level difference,Rate
//
// TYPE:
//   1=experience, 2=item drop
// RACE:
//   0=Formless, 1=Undead, 2=Brute, 3=Plant, 4=Insect,
//   5=Fish, 6=Demon, 7=Demi-Human, 8=Angel, 9=Dragon, 
//   10=Boss monsters, 11=Normal monsters
//
// Note: RENEWAL_DROP and/or RENEWAL_EXP must be enabled.

// Every level below the monster level causes +1% exp (min 5 levels above)
// Every 5 levels above the monster level causes -3% exp
// Bonus are capped at 25 levels difference - more than that is "strange gameplay".
// The penalty is capped at 50 levels difference and serves to push players into
// killing monsters same level or stronger than the players themselves ;-)
1,12,100,140
1,12,50,135
1,12,25,130
1,12,24,129
1,12,23,128
1,12,22,127
1,12,21,126
1,12,20,125
1,12,19,123
1,12,18,122
1,12,17,121
1,12,16,120
1,12,15,119
1,12,14,117
1,12,13,116
1,12,12,115
1,12,11,114
1,12,10,113
1,12,9,111
1,12,8,110
1,12,7,109
1,12,6,108
1,12,5,107
1,12,4,105
1,12,3,104
1,12,2,103
1,12,1,101
1,12,0,100
1,12,-1,100
1,12,-10,95
1,12,-15,91
1,12,-20,88
1,12,-25,85
1,12,-30,82
1,12,-35,79
1,12,-40,76
1,12,-45,73
1,12,-50,70
1,12,-60,65
1,12,-70,60
1,12,-80,55
1,12,-90,50
1,12,-100,40
1,12,-120,35
1,12,-150,30

// After 50 levels difference, drop rate starts getting nerfed... A tiny bit.
2,12,50,105
2,12,40,104
2,12,30,103
2,12,20,102
2,12,10,101
2,12,0,100
2,12,-50,90
2,12,-75,80
2,12,-100,75
2,12,-125,70
2,12,-150,65
2,12,-200,60
2,12,-250,50

