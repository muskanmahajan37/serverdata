// TMW2 scripts.
// Author:
//    Saulc
//    Jesusalva
// Description:
//    Ched is from a quest.
//    But as he is now unused, he'll become someone else on summer.

003-1,62,48,0	script	Ched	NPC_PLAYER,{
    function	ValidSSC {
        return countitem(CactusCocktail)+countitem(CherryCocktail)+countitem(AppleCocktail);
    }
    function	ScoreSSC {
        mesc l("All leaderboards are refreshed hourly."), 1;
        mesc l("Your current score: @@", getq2(SQuest_Ched)), 3;
        mes "";
        mes b(l("Top 10 - Summer Ched's Event"));
	    mes("1."+$@ched_name$[0]+" ("+$@ched_value[0]+")");
	    mes("2."+$@ched_name$[1]+" ("+$@ched_value[1]+")");
	    mes("3."+$@ched_name$[2]+" ("+$@ched_value[2]+")");
	    mes("4."+$@ched_name$[3]+" ("+$@ched_value[3]+")");
	    mes("5."+$@ched_name$[4]+" ("+$@ched_value[4]+")");
	    mes("6."+$@ched_name$[5]+" ("+$@ched_value[5]+")");
	    mes("7."+$@ched_name$[6]+" ("+$@ched_value[6]+")");
	    mes("8."+$@ched_name$[7]+" ("+$@ched_value[7]+")");
	    mes("9."+$@ched_name$[8]+" ("+$@ched_value[8]+")");
	    mes("10."+$@ched_name$[9]+" ("+$@ched_value[9]+")");
        next;
    }
    function	InfoSSC {
        mesc l("@@ - @@ point(s)", getitemlink(CactusCocktail), "1");
        mesc l("@@ - @@ point(s)", getitemlink(CherryCocktail), "3");
        mesc l("@@ - @@ point(s)", getitemlink(AppleCocktail), "5");
        next;
        mes ".:: " + l("Prizes") + " ::.";
        mes getitemlink(MasterBola);
        mesc l("Min. Position: ")+l("top 1"), 3;
        mesc l("Min. Score: "+1000);
        mes "";
        mes getitemlink(PiouBola);
        mesc l("Min. Position: ")+l("top 3"), 3;
        mesc l("Min. Score: "+1000);
        mes "";
        mes getitemlink(SnakeBola);
        mesc l("Min. Position: ")+l("top 5"), 3;
        mesc l("Min. Score: "+700);
        mes "";
        mes getitemlink(TulimsharBola);
        mesc l("Min. Position: ")+l("top 7"), 3;
        mesc l("Min. Score: "+400);
        mes "";
        mes getitemlink(PurpleBola);
        mesc l("Min. Position: ")+l("top 10"), 3;
        mesc l("Min. Score: "+200);
        mes "";
        mes getitemlink(CandorBola);
        mesc l("Min. Position: ")+l("any"), 3;
        mesc l("Min. Score: "+100);
        mes "";
        mes getitemlink(KidBola);
        mesc l("Min. Position: ")+l("any"), 3;
        mesc l("Min. Score: "+25);
        next;
    }
    function	DepositSSC {
    .@pts=.@pts+countitem(CactusCocktail)*1;
    .@pts=.@pts+countitem(CherryCocktail)*3;
    .@pts=.@pts+countitem(AppleCocktail)*5;

    delitem CactusCocktail, countitem(CactusCocktail);
    delitem CherryCocktail, countitem(CherryCocktail);
    delitem AppleCocktail, countitem(AppleCocktail);

    getexp rand2(.@pts-1, .@pts*11/10), rand2(0,.@pts/25);

    setq2 SQuest_Ched, @ched+.@pts;
    @ched=getq2(SQuest_Ched);
    mesc l("Gained @@ points.", .@pts), 3;
    next;
    mesc l("Your Score: @@", @ched), 1;
    mes "";
    ScoreSSC;
    closedialog;
    goodbye;
    close;
    }

    // Begin: Ched
    .@year=getq(SQuest_Ched);
    if (.@year != (gettime(GETTIME_YEAR)-2000))
        setq SQuest_Ched, (gettime(GETTIME_YEAR)-2000), 0, 0;

    @ched=getq2(SQuest_Ched);
    .@claimed=getq3(SQuest_Ched); // Required to prevent rewriting scoreboards

    if (season() == SUMMER && !$@GM_OVERRIDE) goto L_Summer;
    if ((season() == AUTUMN && !.@claimed)) goto L_Autumn;
    if (rand(0,10) == 6)
        npctalk3("I wanted to go to the beach, but I can't find the cave entrance. They told me to look around here... What am I doing wrong?");
    else
        hello;
    end;

// Summer Event
L_Summer:
    if (BaseLevel < 25) {
        mesn;
        if (rand2(0,10) == 6)
            mesq l("I wanted to go to the beach, but I can't find the cave entrance. They told me to look around here... What am I doing wrong?");
        else
            mesq l("Get Rekt Noob.");
        close;
    }

    // Main Core
    do
    {
        mesn;
        mesc l("Current score: @@", @ched), 1;
        mesc l("Thus far you have collected @@ @@, @@ @@ and @@ @@.", countitem(CactusCocktail), getitemlink(CactusCocktail), countitem(AppleCocktail), getitemlink(AppleCocktail), countitem(CherryCocktail), getitemlink(CherryCocktail)), 2;
        mesc l("You can convert these items in event points and claim rewards at autumn."), 2;
        next;
        select
            l("Scoreboards"),
            l("Information"),
            rif(ValidSSC(), l("Deposit all")),
            l("Abort");
        mes "";
        if (@menu == 1)
            ScoreSSC;
        if (@menu == 2)
            InfoSSC;
        if (@menu == 3)
            DepositSSC;

    } while (@menu < 3);
    close;

// Summer Quest Claim Rewards Time
L_Autumn:
    mesc l("Your Score: @@", @ched), 1;
    mes "";
    ScoreSSC;

    // Ensure you have free space on your inv.
    inventoryplace NPCEyes, 1;

    // Are you entitled for a Boia?
    if (@ched >= 25) {
        mesc l("Boias, unlike common shields, does not have any penalty!");
        mesc l("They are filled with a strange gas which makes they deflect attacks. They are done from a material which cannot be cut easily.");
        mesc l("They can be a bit lacking in defensive power, however.");
        mes "";
    }

    .@pos=array_find($@ched_name$, strcharinfo(0));
    .@pos=(.@pos >= 0 ? .@pos+1 : 0);

    // Give you the due boia
    if (.@pos <= 1 && @ched > 1000)
        getitem MasterBola, 1;
    else if (.@pos <= 3 && @ched >= 1000)
        getitem PiouBola, 1;
    else if (.@pos <= 5 && @ched >= 700)
        getitem SnakeBola, 1;
    else if (.@pos <= 7 && @ched >= 400)
        getitem TulimsharBola, 1;
    else if (.@pos <= 10 && @ched >= 200)
        getitem PurpleBola, 1;
    else if (@ched >= 100)
        getitem CandorBola, 1;
    else if (@ched >= 25)
        getitem KidBola, 1;

    // Give extra on GP and EXP rewards on 2018 Summer due extensive amount of bugs
    if (gettime(7) == 2018)
        @ched=@ched*12/10;

    // Give you experience and money reward.
    // Each cocktail is worth 15~30 gp, so we'll give 10 GP per point
    Zeny+=@ched*10;
    getexp BaseLevel*@ched, @ched;

    // The quest is complete for the year.
    setq3 SQuest_Ched, 1;
    closedialog;
    goodbye;
    close;

OnInit:
    .@npcId = getnpcid(.name$);
    setunitdata(.@npcId, UDT_HEADTOP, SamuraiHelmet);
    setunitdata(.@npcId, UDT_HEADMIDDLE, SilkRobe);
    setunitdata(.@npcId, UDT_HEADBOTTOM, NPCEyes);
    setunitdata(.@npcId, UDT_WEAPON, CandorBoots); // Boots
    setunitdata(.@npcId, UDT_HAIRSTYLE, 7);
    setunitdata(.@npcId, UDT_HAIRCOLOR, 17);

    .sex = G_MALE;
    .distance = 5;
    end;
}
