// TMW2 scripts.
// Author:
//    Saulc
//    Jesusalva
// Inac Give a sharp knife for 20 Piou Feathers to player over level 12,
// before Tycoon give it but only bafter level 18 that was too high !

003-1,55,82,0	script	Inac	NPC_PLAYER,{

    .@Inac = getq(TulimsharQuest_Inac);
    if (BaseLevel < 12) goto L_TooWeak;
    if (.@Inac == 1) goto L_Check;
    if (.@Inac == 2) goto L_Complete;

L_GiveTask:
    mesn;
    mesq lg("Hello, wanderer! Welcome to Tulimshar.");
    next;
    mesq l("Was it Nard who brought you here?");
    next;

    menu
        l("Yeah, Nard's sailors saved my life and brought me here!"), L_Quest,
        l("I need to go."), L_Quit;

L_Quest:
    mes "";
    mesn;
    mesq l("Quite benevolent, they are. Nard likes to help anyone who works hard. Anyway, there was something important I needed to say.");
    next;
    mesn;
    .@g$=lg("girl", "boy");
    mesq l("Tulimshar is surrounded by strong monsters. If you go out with your poor equipment, you're going to be dead in no time, @@.", .@g$);
    tutmes l("You can use \"@monsterinfo <monster name>\" to gauge a monster strength."), l("Protip"), false;
    next;
    mesn;
    mesq l("I probably can give you my Sharp Knife, though! If you want to help me, that is.");
    next;

    menu
        l("Really? That would be great!"), L_Start,
        l("I better do this some other time..."), L_Quit;


L_Start:
    setq TulimsharQuest_Inac, 1;
    mes "";
    mesn;
    mesq l("Well, if you come from Candor, you probably will have some fluffy's yellow feathers. I use them to make pillows.");
    goto L_List;

L_Quit:
    mes "";
    mesn;
    mesq l("Have a good day, then.");
    close;

L_List:
    mes "";
    mesn;
    mes l("I can give you my knife if you bring me this:");
    mes l("@@/20 @@", countitem(PiouFeathers), getitemlink(PiouFeathers));
    close;

L_Check:
    mesn;
    mesq l("Did you brought me the feathers?");
    next;
    menu
        l("Yes! Here you are."), L_Give,
        l("I forgot what you need!"), L_List,
        l("Later."), L_Quit;

L_Give:
    if (
        countitem(PiouFeathers)         < 20
        ) goto L_Lying;

    inventoryplace SharpKnife, 1;

    delitem(PiouFeathers, 20);

    getitem(SharpKnife, 1);
    getexp(200, 5);
    setq(TulimsharQuest_Inac, 2);

    mes "";
    mesn;
    mesq l("Enjoy this new weapon, I hope you're successful.");
    close;

L_Complete:
    mesn;
    mesq l("Many stories are told about this city sewers.");
    next;
    mesn;
    mesq l("One of them is really crazy, though: They say that there is a legendary monster down there.");
    next;
    mesn;
    mesq l("That's just nonsense though, I've been there thousands of times and saw no such thing.");
    if (getq(TulimsharQuest_Sewers) > 0)
        close;
    next;
    mesn;
    mesq l("Anyway, there's a barrier to prevent monsters from attacking the city from below.");
    next;
    mesn;
    mesq l("To enter on them, you need to be level 25 and use the following chant: \"Blame Saulc\". I don't know why, but that open doors!");
    setq TulimsharQuest_Sewers, 1;
    close;

L_Lying:
    mesn;
    mesq l("No no no, that's wrong.");
    next;
    mesc l("I need more feathers for a fair deal.");
    next;
    goto L_List;

L_TooWeak:
    mesn;
    mesq l("Hello little kid. Watch out, there's a maggot behind you %%a !");
    tutmes l("You need at least level %s to do this quest.", b(12)), l("WARNING"), false;
    close;

OnShout:
    npctalk3 l("Ahoy! Hey, you, new person! Could you come here?!");
    end;

OnInit:
    .@npcId = getnpcid(.name$);
    setunitdata(.@npcId, UDT_HEADTOP, Bandana);
    setunitdata(.@npcId, UDT_HEADMIDDLE, VneckJumper);
    setunitdata(.@npcId, UDT_HEADBOTTOM, CottonTrousers);
    setunitdata(.@npcId, UDT_WEAPON, LousyMoccasins); // Boots
    setunitdata(.@npcId, UDT_HAIRSTYLE, 7);
    setunitdata(.@npcId, UDT_HAIRCOLOR, 17);

    .sex = G_MALE;
    .distance = 4;
    end;
}
