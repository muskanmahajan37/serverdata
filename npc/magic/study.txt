// TMW2 script
// Author: Jesusalva <admin@tmw2.org>
//
// Magic Script: TMW2_STUDY
//
// Skill to study a target monster
// Will report the monster exact current stats, and is part of research

function	script	SK_study	{
	.@mobGD=getarg(0);
    if (.@mobGD <= 0)
        return;

    // We want monsters
	if (getunittype(.@mobGD) != UNITTYPE_MOB) {
		dispbottom l("This skill can only be used on monsters!");
		return;
	}
	.@mobID=getunitdata(.@mobGD, UDT_CLASS);

    // Research Points
    if (array_rfind(@study, .@mobGD) < 0) {
        .@mult=max(1, 11-getskilllv(TMW2_STUDY));
        .@rp=getmonsterinfo(.@mobID, MOB_LV)/.@mult;
        array_push(@study, .@mobGD);
        if (.@rp) {
            MAGIC_RP+=.@rp;
            dispbottom l("Research Points +%d", .@rp);
        }
    }

    // Report
	dispbottom l("%s - %s/%s HP, %s/%s MP",
				 getmonsterinfo(.@mobID, MOB_NAME),
				 fnum(getunitdata(.@mobGD, UDT_HP)),
				 fnum(getunitdata(.@mobGD, UDT_MAXHP)),
				 fnum(getunitdata(.@mobGD, UDT_SP)),
				 fnum(getunitdata(.@mobGD, UDT_MAXSP)));

    // Truncate.
    // We're saving the GID so it must be "big enough"
    // But not too big so rfind() is not expensive
    if (getarraysize(@study) > 99) {
        deletearray(@study, 30);
    }
	return;
}

