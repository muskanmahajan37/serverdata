// This file is generated automatically. All manually added changes will be removed when running the Converter.
// Map 004-1: Tulimshar mobs
004-1,84,66,33,48	monster	Maggot	1030,47,35000,420000
004-1,44,26,7,5	monster	Croc	1006,3,25000,60000
004-1,38,77,8,21	monster	Scorpion	1071,20,35000,270000
004-1,108,65,10,7	monster	Giant Maggot	1031,4,35000,270000
004-1,106,114,11,7	monster	Golden Scorpion	1078,1,99000,300000
004-1,76,102,18,16	monster	Fire Goblin	1067,7,45000,45000
004-1,40,106,19,12	monster	Red Scorpion	1072,3,95000,60000
