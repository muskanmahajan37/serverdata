// TMW2 Script
// Author:
//    Jesusalva, 4144
// Description:
//  Luanna is responsible for marriage. Subject to change stuff later.

009-5,32,29,0	script	Luanna	NPC_FEMALE,{
    marriagemain();
    close;

OnTimer30000:
    marriagecheck();
    end;

OnInit:
    .@npcId = getnpcid(.name$);
    setunitdata(.@npcId, UDT_HEADTOP, NPCEyes);
    setunitdata(.@npcId, UDT_HEADMIDDLE, VneckJumper);
    setunitdata(.@npcId, UDT_HEADBOTTOM, RaidTrousers);
    setunitdata(.@npcId, UDT_WEAPON, LousyMoccasins); // Boots
    setunitdata(.@npcId, UDT_HAIRSTYLE, 10);
    setunitdata(.@npcId, UDT_HAIRCOLOR, 5);

    .sex = G_FEMALE;
    .distance = 5;
    initnpctimer;

    end;

}
