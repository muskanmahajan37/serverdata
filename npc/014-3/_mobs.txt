// This file is generated automatically. All manually added changes will be removed when running the Converter.
// Map 014-3: Woodlands Central Area mobs
014-3,115,75,91,52	monster	Mouboo	1023,22,30000,35000
014-3,63,74,41,48	monster	Forest Mushroom	1060,10,60000,35000
014-3,104,102,90,35	monster	Squirrel	1032,26,30000,32000
014-3,44,82,37,32	monster	Centaur	1139,3,80000,60000
014-3,164,75,31,14	monster	Pinkie	1132,9,30000,20000,Oscar::OnKillPinkie
014-3,95,65,49,25	monster	Poison Spiky Mushroom	1043,11,45000,25000
014-3,133,89,49,25	monster	Poison Spiky Mushroom	1043,7,45000,45000
014-3,128,112,70,21	monster	Mauve Plant	1135,5,90000,90000
014-3,44,82,37,32	monster	Chagashroom Field	1128,4,90000,90000
014-3,95,65,49,25	monster	Plushroom Field	1011,4,90000,90000
