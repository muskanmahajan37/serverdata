// TMW-2 Script
// Author:
//  Jesusalva
// Description:
//  This NPC is a work on progress. It measures all players equal.
// Controls the second floor

// 21: From 2 to 1
008-2,135,19,0	script	#FPDM21	NPC_HIDDEN,0,0,{
    end;
OnTouch:
    // Party leader cannot retreat
    if (strcharinfo(0) == getpartyleader(getcharid(1)))
        dispbottom l("As the leader of your party, you can only go forward.");
    else
        warp "008-1", 43, 104;
    end;
}

008-2,135,19,0	script	#FPDM23	NPC_HIDDEN,0,0,{
    end;
OnTouch:
    end;
    // IMPORTANT: Prevent party master from returning here
    getmapxy(.@m$, .@x, .@y, 0, getpartyleader(getcharid(1)));
    if (.@m$ ~= "008-*" &&
        .@m$ != "008-0" &&
        .@m$ != "008-1" &&
        .@m$ != "008-2" &&
        BaseLevel > 40 &&
        mobcount("008-2", "Second Dungeon Master::OnBossDeath") == 0 &&
        @pmloop) {
        dispbottom l("Go and follow @@, your party leader!", getpartyleader(getcharid(1)));
        warp "008-1", 176, 20; // 008-2
    } else if (BaseLevel > 40) {
        dispbottom l("If I bring @@, my party leader, and the boss is defeated, I can go you in.", getpartyleader(getcharid(1)));
    } else {
        mesc l("I cannot pass, because I am only level @@.", BaseLevel);
        mesc l("Newbies can only get past their limits once, with their party leader level help! If they die, they can't rejoin!");
        // I thought it would be better this way. There was at least three different ways to prevent that.
    }
    end;
}

008-2,75,240,0	script	Second Dungeon Master	NPC_BRGUARD_SWORD,{
    // Double-check, you should have been flung out long ago.
    if (getcharid(1) <= 0 || @pmloop <= 0) {
        mesn;
        mesq l("Something seems wrong.");
        warp "SavePoint",0,0;
        close;
    }
    if (mobcount("008-2", "Second Dungeon Master::OnBossDeath") > 0) {
        npctalk3 l("What are you doing? Fight!");
        end;
    }

    mesn;
    mesq l("Compliments reaching this far, @@ from the @@ party!", strcharinfo(0), getpartyname(getcharid(1)));
    next;

    mesn;
    mesq l("I can summon the BOSS for the level 21~40 area.");
    next;
    if (strcharinfo(0) != getpartyleader(getcharid(1))) goto L_NotYou;
    if (BaseLevel < 40) goto L_TooWeak;
    mesn;
    mesq l("He is strong though, so keep your party togheter!");
    next;
    // Get info about your party, and backup it
    getpartymember(getcharid(1));
	.@count = $@partymembercount;
	copyarray(.@name$[0], $@partymembername$[0], $@partymembercount);
	copyarray(.@aid[0], $@partymemberaid[0], $@partymembercount);
	copyarray(.@cid[0], $@partymembercid[0], $@partymembercount);
    .@count_online=0;

    if (.@count < 3 && !$@GM_OVERRIDE) goto L_TooSmall;
    mesn;
    mesc l("Are you and your party ready?"), 1;
    if (askyesno() != ASK_YES)
        close;

    // Loop though party to see if you can start
	for (.@i = 0; .@i < .@count; ++.@i) {
        // Online?
		if (isloggedin(.@aid[.@i], .@cid[.@i])) {
            getmapxy(.@m$, .@x, .@y, 0, .@name$[.@i]);
            // Here?
            if (.@m$ == .map$)
    			.@count_online++;
        }
    }

    // How many are logged in and here?
    if (.@count_online < 3 && !$@GM_OVERRIDE) goto L_TooSmall;

    // TODO: Query if exp sharing is enabled
    // TODO: Instance for party
    // TODO: Second Floor
    npctalk l("@@: Fight!", getpartyname(getcharid(1)));
    monster "008-2", rand(74,84), rand(241,250), "Second Dungeon Boss", BanditLord, 1, "Second Dungeon Master::OnBossDeath";
    close;










// Only the party leader can start this.
L_NotYou:
    mesn;
    dispbottom l("Go and fetch @@, the party leader!", getpartyleader(getcharid(1)));
    close;

// Minimum 3 players
L_TooSmall:
    mesn;
    mesq l("However, I need to see at least three volunteers here, to allow you in.");
    close;

// Must have level to face boss
L_TooWeak:
    mesn;
    mesq l("This is the end for your party, as the leader doesn't have sufficient level.");
    close;

// Second Floor special monsters
OnReward:
    // 40% odds of a Black Scorpion coming to avenge fallen monster leader
    if (rand(1,5) % 2) {
        getmapxy(.@m$, .@x, .@y, 0);
        monster .@m$, .@x, .@y, "Semi Boss", BlackScorpion, 1;
    }
    Zeny=Zeny+700;
    getexp 400, 60;
    getitem CelestiaTea, 1;
    dispbottom l("Reward: 700 GP, 400 XP, 1 @@", getitemlink(CelestiaTea));


OnBossDeath:
    // Give every party member in map a reward
    partytimer("008-2", 200, "Second Dungeon Master::OnReward", getcharid(1));

    // Warp everyone and add timers
    warpparty("008-1", 176, 20, getcharid(1), "008-2", true); // TODO 008-3
    // While we can't advance, we allow you to redo the dungeons

    /*
    // Bonus Monsters
    monster("008-3", 104, 238, "Time Bonus", RobinBandit, 1, "Party Master::OnMobTime");
    monster("008-3", 85, 117, "Time Bonus", RobinBandit, 1, "Party Master::OnMobTime");

    // Five Chests
    monster("008-3", 38, 104, "Mysterious Chest", any(BronzeChest, BronzeMimic), 1);
    monster("008-3", 41, 104, "Mysterious Chest", any(BronzeChest, BronzeMimic, EvilChest), 1);
    monster("008-3", 44, 104, "Mysterious Chest", any(SilverChest, SilverMimic), 1);
    monster("008-3", 47, 104, "Mysterious Chest", any(BronzeChest, BronzeMimic, EvilChest), 1);
    monster("008-3", 50, 104, "Mysterious Chest", any(BronzeChest, BronzeMimic), 1);
    */
    end;

OnInit:
    .sex = G_MALE;
    .distance = 5;
    end;
}

