// TMW2 Script
// Authors:
//    Jesusalva
// Description:
//    Link portals to soul menhirs like the teleporters from old
//    The price is temporary. This feature got in because no ship in Nivalis Port
//    PS. Anise => “Aisen” Anagram


024-1,155,80,0	script	#WarpGateFrost	NPC_NO_SPRITE,1,0,{
    end;

OnTouch:
    TeleporterGate(TP_FROST);
    close;


OnInit:
    .sex = G_OTHER;
    .distance = 1;
    end;
}

