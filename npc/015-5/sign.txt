// TMW2 Script.
// Author:
//    Jesusalva
// Description:
//    You're at a PVP Area

015-5,356,61,0	script	WARNING#015535661	NPC_SWORDS_SIGN2,{
    mesn;
    mesc l("You are entering on a PVP Area with lowered death penalty.");
    next;
    mesn;
    mesc l("The mines beyond this point have been shut at 280 AT because Terranite. You've been warned!");
    close;

OnInit:
    .sex = G_OTHER;
    .distance = 3;
    end;
}

015-5,350,61,0	duplicate(WARNING#015535661)	WARNING#015535061	NPC_SWORDS_SIGN2

