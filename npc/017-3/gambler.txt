// TMW2 Scripts
// Author:
//    Jesusalva
// Description:
//    Gambler: Can you remember the sequence?

017-3,23,38,0	script	Gambler#017-3	NPC_PLAYER,{
    function colorname {
        switch (getarg(0)) {
        case 1:
            return "Green"; break;
        case 2:
            return "Blue"; break;
        case 3:
            return "Red"; break;
        case 4:
            return "Yellow"; break;
        case 5:
            return "Exit"; break;
        default:
            return l("ERROR: %d", getarg(0));
        }
    }

    goto L_Menu;

L_Menu:
    showavatar NPC_FLOPPED_NOBLEMAN;
    mesn;
    mesc l("Gambling is for the weak, I offer you a true game!");
    mesc l("You need %d %s. I'll start showing you sequences of colors.", .price, getitemlink(CasinoCoins));
    mesc l("The farther you go on the sequence, the better the payout!");
    next;
    menu
        rif(countitem(CasinoCoins) >= .price, l("Let's play!")), L_Start,
        l("Information"), L_Info,
        l("Leave"), -;
    close;

L_Info:
    mes "";
    mesc l("Rules:");
    mesc l("A color sequence will be displayed on the avatar frame.");
    mesc l("You must then repeat the sequence at the board which will show.");
    next;
    mesc l("Prizes:");
    mesc l("You'll get %d GP every time you finish the sequence.", .prize);
    next;
    mesc l("Winning Strike Prizes:");
    mesc l("Every %d sequences, you'll get a %s!", 10, getitemlink(StrangeCoin));
    mesc l("If you get %d sequence, you'll get a %s!", 30, getitemlink(BronzeGift));
    mesc l("If you get %d sequence, you'll get a %s!", 50, getitemlink(SilverGift));
    //mesc l("If you get %d sequence, you'll get a %s!", 50, getitemlink(GoldenGift));
    next;
    goto L_Menu;


L_Start:
    showavatar AVATAR_SEQBOARD;
    mesc l("Pay attention to the sequence!");
    next;
    delitem CasinoCoins, .price;
    deletearray(@sequence);
    @streak=0;

L_Sequence:
    // Configure
    setnpcdialogtitle l("Memorize the sequence!");
    array_push(@sequence, 1+rand2(4));
    sleep2(1000);

    // Display
    freeloop(true);
    for (.@i=0;.@i < getarraysize(@sequence);.@i++) {
        showavatar 1200+@sequence[.@i];
        sleep2(1200-(@streak*20));
    }
    freeloop(false);

    // Request
    setnpcdialogtitle l("What was the sequence?");
    showavatar AVATAR_SEQBOARD;
    sleep2(500);

    for (.@i=0;.@i < getarraysize(@sequence);.@i++) {
        setskin "seqboard";
        select
            l("Green"),
            l("Blue"),
            l("Red"),
            l("Yellow"),
            l("Exit");
        .@ans=@menu;
        setskin "";
        mes "";
        mes l("%s", colorname(.@ans));
        //next;
        setnpcdialogtitle strnpcinfo(1);

        // Exit
        if (.@ans == 5)
            goto L_Close;

        // Wrong reply
        if (.@ans != @sequence[.@i])
            goto L_Wrong;
        // Correct!
    }
    mes "";
    showavatar AVATAR_SEQBOARD_WELL;

    // Seems like everything is/was correct
    mesn;
    mesq l("Congratulations! Everything was correct!");
    Zeny+=.prize;
    @streak+=1;

    // Winning Streak
    if (@streak % 10 == 0)
        getitem StrangeCoin, 1;
    if (@streak == 30)
        getitem BronzeGift, 1;
    if (@streak == 50)
        getitem SilverGift, 1;
    mesc l("Your current win streak is @@!", @streak);
    next;
    // Game over
    if (@streak == 50)
        goto L_Close;
    // Otherwise, go ahead
    mesn;
    mesc l("Continue?"), 1;
    next;
    if (askyesno() == ASK_YES)
        goto L_Sequence;
    goto L_Close;

L_Wrong:
    showavatar AVATAR_SEQBOARD_FAIL;
    mesn;
    mesq l("Oh no... That is wrong! %%3");
    next;
    mesn;
    mesq l("Better luck next time!");
    close;

L_Close:
    mesn;
    mesq l("Thanks for playing!");
    close;

OnInit:
    .@npcId = getnpcid(.name$);
    setunitdata(.@npcId, UDT_HEADTOP, TopHat);
    setunitdata(.@npcId, UDT_HEADMIDDLE, CreasedShirt);
    setunitdata(.@npcId, UDT_HEADBOTTOM, JeansShorts);

    .sex = G_MALE;
    .distance = 4;
    .price = 5;
    .prize = 50;
    npcsit;
    end;
}

