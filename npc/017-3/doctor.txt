// TMW2/LoF scripts.
// Authors:
//    TMW-LoF Team
//    Jesusalva
// Description:
//    Part from THE EPISODE quest
// Reference:
//    http://forums.landoffire.org/viewtopic.php?f=7&t=1320&sid=80d2c735b55ccb06a39955a8fbca3913

017-3,75,68,0	script	The Doctor	NPC_LOF_DOCTOR,{
    showavatar NPC_LOF_DOCTOR;
    .@q=getq(LoFQuest_EPISODE);
    if (BaseLevel < 30) goto L_Weak;
    if (.@q == 1) goto L_Check;
    if (.@q == 2 && BaseLevel >= 40) goto L_Miler;
    if (.@q >= 2) goto L_Tea;
    mesn;
    mesq l("Quite interesting, quite interesting indeed.");
    menu
        l("Um, might I ask, what is so interesting?"), L_Intro,
        l("Yes, uhh, very interesting indeed. Haha. I better leave..."), -;
    mes "";
    mesc l("@@ stares you as you slowly get away from him. Odd person.", .name$);
    close;

L_Intro:
    mes "";
    mesn;
    mesq l("Well, you are. You're quite interesting. I've been watching you for some time now, as you've been helping so many people: you're quite a master at what you do, you know.");
    menu
        l("Well, thanks."), L_IntroContinue,
        l("Ok then... Uh... Please excuse me....."), -;
    mes "";
    mesc l("@@ stares you as you slowly get away from him. Odd person.", .name$);
    close;

L_IntroContinue:
    mes "";
    mesn;
    mesq l("I don't suppose you have some herbs and a few bottles of potion with you, do you?");
    menu
        l("'Some herbs and potion'? Could you be more specific?"), L_IntroSpecify,
        l("Certainly not. Uhh, please excuse me."), -;
    mes "";
    mesc l("@@ stares you as you slowly get away from him. Odd person.", .name$);
    close;

L_IntroSpecify:
    mes "";
    mesn;
    mesq l("Ah, sorry, of course. I need quite a few herbs, look:");
    mesc l("@@/150 @@", countitem(MauveHerb), getitemlink(MauveHerb));
    mesc l("@@/150 @@", countitem(CobaltHerb), getitemlink(CobaltHerb));
    mesc l("@@/150 @@", countitem(GambogeHerb), getitemlink(GambogeHerb));
    mesc l("@@/100 @@", countitem(AlizarinHerb), getitemlink(AlizarinHerb));
    mesc l("@@/50 @@", countitem(ShadowHerb), getitemlink(ShadowHerb));
    mesc l("@@/10 @@", countitem(HastePotion), getitemlink(HastePotion));
    menu
        l("That shouldn't been too hard, but do I get something in return?"), L_IntroReward,
        l("O.o \"That's a lot. Maybe another day.\""), -;
    mes "";
    mesc l("@@ stares you as you slowly get away from him. Odd person.", .name$);
    close;

L_IntroReward:
    mes "";
    mesn;
    mesq l("I suppose, what would you like?");
    next;
    mesn;
    mesq l("Er, nevermind, I've thought of something to give you. You can go off now and get what I need.");
    setq LoFQuest_EPISODE, 1;
    close;

L_Check:
    mesn;
    mesq l("Did you brought what I asked for?");
    mesc l("@@/150 @@", countitem(MauveHerb), getitemlink(MauveHerb));
    mesc l("@@/150 @@", countitem(CobaltHerb), getitemlink(CobaltHerb));
    mesc l("@@/150 @@", countitem(GambogeHerb), getitemlink(GambogeHerb));
    mesc l("@@/100 @@", countitem(AlizarinHerb), getitemlink(AlizarinHerb));
    mesc l("@@/50 @@", countitem(ShadowHerb), getitemlink(ShadowHerb));
    mesc l("@@/10 @@", countitem(HastePotion), getitemlink(HastePotion));
    next;
    if (askyesno() != ASK_YES)
        close;
    inventoryplace HerbalTea, 5;
    if (
        countitem(MauveHerb) < 150 ||
        countitem(CobaltHerb) < 150 ||
        countitem(GambogeHerb) < 150 ||
        countitem(AlizarinHerb) < 100 ||
        countitem(ShadowHerb) < 50 ||
        countitem(HastePotion) < 10)
        goto L_Missing;
    delitem MauveHerb, 150;
    delitem CobaltHerb, 150;
    delitem GambogeHerb, 150;
    delitem AlizarinHerb, 100;
    delitem ShadowHerb, 50;
    delitem HastePotion, 10;
    getexp 7995, 0;
    setq LoFQuest_EPISODE, 2;
    getitem HerbalTea, 5;
    mesn;
    mesq l("Mmm, it's been so long since I have had herbal tea. You have my gratitude.");
    next;
    mesn strcharinfo(0);
    mesq l("Seriously? What sort of reward is that?");
    next;
    mesn;
    mesq l("Well, I suppose you can have some of my tea.");
    close;

L_Tea:
    mesn;
    mesq l("If you want, you can bring me some more of those herbs and potions.");
    mesc l("@@/30 @@", countitem(MauveHerb), getitemlink(MauveHerb));
    mesc l("@@/30 @@", countitem(CobaltHerb), getitemlink(CobaltHerb));
    mesc l("@@/30 @@", countitem(GambogeHerb), getitemlink(GambogeHerb));
    mesc l("@@/20 @@", countitem(AlizarinHerb), getitemlink(AlizarinHerb));
    mesc l("@@/10 @@", countitem(ShadowHerb), getitemlink(ShadowHerb));
    mesc l("@@/2 @@", countitem(HastePotion), getitemlink(HastePotion));
    menu
        l("Alright, I have them here!"), L_Check2,
        l("No thanks, see ya!"), -;
    close;

L_Check2:
    inventoryplace HerbalTea, 1;
    if (
        countitem(MauveHerb) < 30 ||
        countitem(CobaltHerb) < 30 ||
        countitem(GambogeHerb) < 30 ||
        countitem(AlizarinHerb) < 20 ||
        countitem(ShadowHerb) < 10 ||
        countitem(HastePotion) < 2)
        goto L_Missing;
    delitem MauveHerb, 30;
    delitem CobaltHerb, 30;
    delitem GambogeHerb, 30;
    delitem AlizarinHerb, 20;
    delitem ShadowHerb, 10;
    delitem HastePotion, 2;
    getexp 122, 12;
    getitem HerbalTea, 1;
    mesn;
    mesq l("Thanks, enjoy your tea! I will certainly enjoy mine!");
    close;

L_Weak:
    mesn;
    mesq l("Hmm, it's very interesting, very ... (mumbling).");
    close;

L_Missing:
    mesn;
    mesq l("Sorry, you do not have enough ingredients. You'd better search thoroughly.");
    close;

/////// Second Act
L_Miler:
    mesn;
    mesq l("Thank you for helping me make my tea. I hope the potions have been helpful...");
    next;
    mesn;
    mesq l("That reminds me. I have a friend in Nivalis named Miler who gave me some hints on the recipe. Would you take him a sample of what I gave you?");
    mesq l("If you've used all the ones I've given, you can always bring me more ingredients.");
    menu
        "I'll go right away.", -,
        "Ah, I suppose I need to gather more ingredients first...", -;
    setq LoFQuest_EPISODE, 3;
    close;



OnInit:
    .sex=G_MALE;
    .distance=5;
    end;

}
