// TMW2/LoF scripts.
// Authors:
//      TMW-LoF Team
//      Jesusalva
//      Saulc
// Description:
//    Weekly Quest

017-3,68,87,0	script	Doug	NPC_PLAYER,{
    if (BaseLevel < 25)
        goto L_Weak;
    .@q2=getq2(LoFQuest_Doug);
    if (.@q2 < santime()) {
        mesn;
        mesq l("This room is too dark. I want to brighten it up.");
        next;
        .@price=(getiteminfo(CaveSnakeLamp, ITEMINFO_SELLPRICE)*3)*8;
        mesn;
        mesq l("I am willing to pay @@ GP for 10 @@ you bring me!", .@price, getitemlink(CaveSnakeLamp));
        next;
        select
            rif(countitem(CaveSnakeLamp) >= 10, l("Here they are!")),
            l("Not now...");
        mes "";
        if (@menu == 1) {
            delitem CaveSnakeLamp, 10;
            Zeny=Zeny+.@price; //864
            getexp (.@price*rand(100,200)/100), 10; // 864 - 1728
            setq2 LoFQuest_Doug, santime()+(60*60*24*7);
            mesn;
            mesq l("Many, many thanks!");
            next;
        }
        mesn;
        mesq l("Too bad these lamps wear off after a while... I am making stocks of them now!");
        close;
    } else {
        mesn;
        mesq l("Thanks for the help!");
        close;
    }

L_Weak:
    mesn;
    mesq l("Heya dude! Don't you think this room is too dark?");
    next;
    mesn;
    mesq l("I am trying to invent the light bulb, but my name is not Thomas...");
    close;

OnInit:
    .@npcId = getnpcid(.name$);
    setunitdata(.@npcId, UDT_HEADTOP, FancyHat);
    setunitdata(.@npcId, UDT_HEADMIDDLE, CreasedShirt);
    setunitdata(.@npcId, UDT_HEADBOTTOM, NPCEyes);
    setunitdata(.@npcId, UDT_WEAPON, JeansShorts);
    setunitdata(.@npcId, UDT_HAIRSTYLE, 2);
    setunitdata(.@npcId, UDT_HAIRCOLOR, 4);

    .sex=G_MALE;
    .distance=5;
    end;
}

