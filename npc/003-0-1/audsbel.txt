// TMW2 scripts.
// Authors:
//    Jesusalva
// Description:
//    One of the Mana Magic Leaders
//    Planned, there are only the five Mana Wizards: Auldsbel (Tulimshar), Sagratha (Woodlands), Morgan (Candor), Blue Sage (Nivalis), Lalica (LoF).

//    Gives #parum
//    Gives #mkpot
//    Quest step 11 doesn't exist - legacy debug

003-0-1,81,27,0	script	Auldsbel	NPC_AULDSBEL,{
    mesn l("Auldsbel the Wizard");
    mesq l("Welcome back, Padric!");
    mesq l("Do you believe I've lost my @@? I can't see anything well without that! %%a", getitemlink(Googles));
    if (countitem(Googles))
        mesq l("I'm not interested in yours, by the way.");
    next;
    mesn;
    mesq l("Anyway, I am one of the few Mana Wizard, and I love Transmutation!");
    next;

L_Main:
    select
        rif(MAGIC_LVL, l("What's the difference from a Mana Wizard and a Mage?")),
        l("Who are you? Where are you from?"),
        rif(MAGIC_LVL, l("How do I advance in Mana Magic? How it works?")),
        rif(MAGIC_LVL, l("What do you know about other Mana Magic Professors?")),
        rif(MAGIC_LVL, l("Can you teach me Mana Magic?")),
        l("Do you still need help with your experiments?"),
        l("Actually, I gotta go, see ya!");

    mes "";
    switch (@menu) {
        case 1: // What's the difference from a Mana Wizard and a Mage?
            if (MAGIC_LVL < 2) {
                mesn;
                mesn;
                mesq l("%%4 Haven't you read your Grimorium yet?!");
                next;
                mesn;
                mesq l("%%@ You can get Magic by joining class and subclass, and with some NPCs. These work out-of-box. Easy.");
                next;
                mesn;
                mesq l("And there is the Mana Magic, for the pro %%e You'll waste your life on that, as it have an experience system.");
                next;
                mesn;
                mesq l("%%G Just like normal magic, you need power from the Mana Stone, which is based on your levels, intelligence, and mana.");
                next;
                mesn;
                mesq l("I like Mana Magic the best, as we can both summon creatures as transmutate stuff. %%N");
                next;
            } else {
                mesn;
                mesq l("Well, long story short, we have two magic systems. One works out of box. The later one, is the Mana Magic.");
                next;
                mesn;
                mesq l("Mana Magic is less reliable and have an experience system. It's the most common for summoning and transmutation.");
                next;
                mesn;
                mesq l("I could explain this better, but you are just a noob at magic, if you understand me.");
                next;
            }
            mesn;
            mesq l("To be honest, you should use both. And remember, all Mana Magic skills can be used like they were @sk-commands. %%H");
            next;
            break;
        case 2: // Who are you? Where are you from?
            mesn;
            mesq l("Well, speaking a bit about myself can't hurt. I am a member from the Magic Council, but one day I decided to take a vacations.");
            next;
            mesn;
            mesq l("I moved to Hurnscald, stayed there for over a decade, until Lord Transmogrifier Pontorias the Plaid (May His Shape Reflect His Soul Forever) died.");
            next;
            mesn;
            mesq l("Then I returned to Tulimshar. Now I do research, vote on the Magic Council meetings, and I also teach young mages about Transmutation magic.");
            next;
            break;
        case 3: // How do I advance in Mana Magic? How it works?
            mesn;
            mesq l("Mana Magic works similar to regular magic: You can find it on your skill window, and can drag it to your shortcut list.");
            next;
            mesn;
            mesq l("To get more experience and magic power, you must practice magical spells.  Make sure to vary them; you will learn nothing if you cast the same spell over and over. Also, spells that consume no components seem not to be very instructive in practice.");
            next;
            mesn;
            mesq l("Usually, you can find magic on these buildings, but watch out, there is magic to be found elsewhere, and some items are bound with it!");
            next;
            mesn;
            mesq l("While on normal magic you must take care with all attributes, on mana magic, you usually only need to worry with reagents and intelligence.");
            next;
            mesn;
            mesq l("Also, unlike regular magic which may cause delay before and after, Mana Magic usually only have a cooldown. But it is hard to know how long that cooldown is..."); // We may use addtimer() on one or other spell.
            next;
            break;
        case 4: // What do you know about Sagratha?
            mesn;
            mesq l("Lemme see... Sagratha is often regarded as a kind and rather powerful elf lady mage... who hate people.");
            next;
            mesn;
            mesq l("And I'm not talking only about humans here! But perhaps, if you build a good reputation, she teaches you something.");
            next;
            mesn;
            mesq l("There's also Morgan, who lives in Candor. She is a Redy, and is married with Zitoni. They are great alchemists.");
            next;
            mesn;
            mesq l("By last, there was the Blue Sage, living on Nivalis... I never met him outside the Council.");
            next;
            mesn;
            mesq l("As you see, you can count the mages with the fingers of one hand. It's not just Mana Magic, either - Magic in overall is almost dead, with almost every Mana Stone on the power of the Monster King.");
            next;
            mesn;
            mesq l("Defeating him would not only stop monster invasions, but it would also bring magic back... And probably another war, over the mana stones. %%S");
            next;
            break;
        // Teaching and helping are bound one to other, to save space on variable e.e
        case 5: // Can you teach me Mana Magic?
        case 6: // Do you still need help with your experiments?
            goto L_Magic;
            break;
        default: // Actually, I gotta go, see ya!
            goodbye;
            closedialog;
            close;
    }
    goto L_Main;

L_Magic:
    .@q=getq(General_Auldsbel);
    switch (.@q) {
    // Help on research to gain his favor
    case 0:
        mesn;
        mesq l("I actually need help. Padric and I were doing some research with catalysts, you see.");
        next;
        mesn;
        mesq l("Now I need 20 @@, 20 @@ and 60 @@ to finish my research. Easy materials, except for the Shadow Herb.", getitemlink(MauveHerb), getitemlink(ShadowHerb), getitemlink(SilkCocoon));
        next;
        mesn;
        mesq l("Shadow Herb only grows on dangerous places, and is mostly found on the Land Of Fire, or underground of very very deep caves.");
        next;
        select
            l("I'll try to find them."),
            l("I actually have them, here.");
        if (@menu == 2) {
            mes "";
            mesn;
            mesq l("Excellent! Let me see...");
            next;
            if (
                countitem(MauveHerb) < 20 ||
                countitem(ShadowHerb) < 20 ||
                countitem(SilkCocoon) < 60)
                    goto L_Lie;
            delitem MauveHerb, 20;
            delitem ShadowHerb, 20;
            delitem SilkCocoon, 60;
            getexp 2500, 0;
            Zeny=Zeny+250;
            setq General_Auldsbel, 1;
            mesn;
            mesq l("Yes, many thanks. This will help me a lot.");
            mesc l("Gained 2500 XP and 250 GP");
        }
        break;
    // Learn #parum
    case 1:
        if (MAGIC_LVL < 1)
            goto L_Magicless;
        mesn;
        mesq l("Hmm, I think I can teach you a basic Mana Skill now. That one is pretty simple.");
        next;
        skill(TMW2_PARUM,1,0);
        setq General_Auldsbel, 2;
        mesn;
        mesq l("This is the @@ skill. It transmutes a single @@ in a @@.", "##B@sk-parum##b", getitemlink(RawLog), getitemlink(MoubooFigurine));
        next;
        mesn;
        mesq l("It may also create some @@ or a @@, with enough skill.", getitemlink(Arrow), getitemlink(WoodenLog));
        next;
        mesn;
        mesq l("So! Please transmute a @@ and bring it to me. You may need to switch with another mana skill, until you are successful.", getitemlink(MoubooFigurine));
        break;
    // Bring the Mouboo figurine back
    case 2:
        mesn;
        mesq l("Have you managed to transmute the @@ I asked for?", getitemlink(MoubooFigurine));
        next;
        if (askyesno() == ASK_YES) {
            if (!countitem(MoubooFigurine)) goto L_Lie;
            if (!MAGIC_EXP) goto L_Lame;
            delitem MoubooFigurine, 1;
            getexp 5000, 0;
            setq General_Auldsbel, 3;
            mesn;
            mesq l("Very well - Congratulations! That was very easy, though, and this one is full of imperfections.");
            mesc l("Gained 5000 XP");
            next;
            mesn;
            mesq l("That skill was only to allow you to practice. Now listen well: Transmutation is ##BNOT##b crafting!");
            next;
            mesn;
            mesq l("Magic is sacred. With transmutation, you can create convenience items, specially reagents for other magic skills.");
            next;
            mesn;
            mesq l("But it will not help you to craft something as complex as weapons or armors. If we catch you profaning this magic... I'll have you returned to the sea %%e");
            next;
            mesn;
            mesq l("This is just a friendly advise. We don't take magic lightly. And you shouldn't, either.");
        }
        break;
    // Help on research to gain his favor
    case 3:
        mesn;
        mesq l("I actually need help. I am a mage, and I'm feeling lazy to get the stuff I need.");
        next;
        mesn;
        mesq l("You don't seem to have anything better to do, anyway.");
        mesq l("Now please bring me @@/2 @@, @@/20 @@, @@/30 @@ and @@/70 @@ so I don't need to leave here and start travelling everywhere...", countitem(IcedBottle), getitemlink(IcedBottle), countitem(Root), getitemlink(Root), countitem(Potatoz), getitemlink(Potatoz), countitem(Moss), getitemlink(Moss));
        next;
        select
            l("I'll try to find them."),
            l("I actually have them, here.");
        if (@menu == 2) {
            mes "";
            mesn;
            mesq l("Excellent! Let me see...");
            next;
            if (
                countitem(IcedBottle)   <  2 ||
                countitem(Root)         < 20 ||
                countitem(Potatoz)      < 30 ||
                countitem(Moss)         < 70)
                    goto L_Lie;
            delitem IcedBottle,  2;
            delitem Root,       20;
            delitem Potatoz,    30;
            delitem Moss,       70;
            getexp 28692, 0;
            Zeny=Zeny+550;
            setq General_Auldsbel, 4;
            mesn;
            mesq l("Yes, many thanks. This will help me a lot.");
        }
        break;
    // Learn #mkpot
    case 4:
        if (MAGIC_LVL < 2)
            goto L_Magicless;
        mesn;
        mesq l("Hmm, I think I can teach you a basic Mana Skill now. This one is more advanced, though.");
        next;
        skill(TMW2_TRANSMIGRATION,1,0);
        setq General_Auldsbel, 5;
        mesn;
        mesq l("This is the @@ skill. It transmutes stuff into other stuff. I'll teach you some more recipes as class drag on.", "##B@sk-trans##b");
        next;
        mesn;
        mesq l("It may fail, and you might end up with something entirely unexpected, or nothing at all!");
        next;
        mesn;
        mesq l("Unlike Parum, you can use the skill points you get every time your job level rises. That will lower the mana cost and increase success chances.");
        next;
        mesn;
        mesq l("But please be picky with how you spend job points. They are hard to come by, and I'm not entirely sure you can change it later.");
        next;
        mesn;
        mesq l("Also, job levels get really hard to obtain after a while. The decision is up to you, just be aware there's that possibility.");
        break;
    // Help on research to gain his favor
    case 5:
        mesn;
        mesq l("Well, I decided to resume an old research of mine, now that I have a minion (you) to gather stuff for me.");
        next;
        mesn;
        mesq l("Now please be a good helper, and aid me by bringing:");
        mesc l("* @@/@@ @@", countitem(HastePotion), 10, getitemlink(HastePotion));
        mesc l("* @@/@@ @@", countitem(StrengthPotion), 10, getitemlink(StrengthPotion));
        mesc l("* @@/@@ @@", countitem(HerbalTea), 10, getitemlink(HerbalTea));
        mesc l("* @@/@@ @@", countitem(RedScorpionStinger), 25, getitemlink(RedScorpionStinger));
        mesc l("* @@/@@ @@", countitem(SilkCocoon), 100, getitemlink(SilkCocoon));
        next;
        select
            l("I'll try to find them."),
            l("I actually have them, here.");
        if (@menu == 2) {
            mes "";
            mesn;
            mesq l("Excellent! Let me see...");
            next;
            if (!transcheck(
                HastePotion, 10,
                StrengthPotion, 10,
                HerbalTea, 10,
                RedScorpionStinger, 25,
                SilkCocoon, 100))
                    goto L_Lie;
            getexp 7500, 0;
            Zeny=Zeny+1250;
            setq General_Auldsbel, 6;
            mesn;
            mesq l("Thanks. I'm actually conducting experiments with scorpions. Please come back later.");
            mesc l("Gained 7500 XP and 1250 GP");
        }
        break;
    // Learn transmigration: Scorpion Stinger and Claw
    case 6:
        if (MAGIC_LVL < 2)
            goto L_Magicless;
        mesn;
        mesq l("Well, I'm currently researching scorpions, as you can imagine.");
        next;
        setq General_Auldsbel, 7;
        mesn;
        mesq l("I'll teach you how to transmute some parts of theirs. You can use it to convert a @@ into a @@, but not the other way around, for example.", getitemlink(BlackScorpionStinger), getitemlink(RedScorpionStinger));
        next;
        mesn;
        mesq l("This have many uses. Maybe. Anyway, I'm soon done with my experiment, so please come back later.");
        close; // On purpose
        break;
    // Help on research to gain his favor
    case 7:
        mesn;
        mesq l("Hmm... See, the thing is that transmuting living beings is not normally something that transmutation magic can do.");
        next;
        mesn;
        mesq l("But I will not give up on my little experiment. Incidentally, Snakes are shaddy enough for my experiment. I promise you, I'll succeed this time.");
        next;
        mesn;
        mesq l("Oh. And don't mention anyone what I'm researching here. No need to fuss over minor things, don't you agree? It's totally not shaddy. Not shaddy at all!");
        next;
        mesn;
        mesq l("Now please be a good helper, and aid me by bringing:");
        mesc l("* @@/@@ @@", countitem(MountainSnakeTongue), 15, getitemlink(MountainSnakeTongue));
        mesc l("* @@/@@ @@", countitem(SnakeTongue), 15, getitemlink(SnakeTongue));
        mesc l("* @@/@@ @@", countitem(CaveSnakeTongue), 15, getitemlink(CaveSnakeTongue));
        mesc l("* @@/@@ @@", countitem(MountainSnakeEgg), 15, getitemlink(MountainSnakeEgg));
        mesc l("* @@/@@ @@", countitem(SnakeEgg), 15, getitemlink(SnakeEgg));
        mesc l("* @@/@@ @@", countitem(CaveSnakeEgg), 15, getitemlink(CaveSnakeEgg));
        next;
        select
            l("I'll try to find them."),
            l("I actually have them, here.");
        if (@menu == 2) {
            mes "";
            mesn;
            mesq l("Excellent! Let me see...");
            next;
            if (!transcheck(
                MountainSnakeTongue, 15,
                SnakeTongue, 15,
                CaveSnakeTongue, 15,
                MountainSnakeEgg, 15,
                SnakeEgg, 15,
                CaveSnakeEgg, 15))
                    goto L_Lie;
            getexp 20000, 0;
            Zeny=Zeny+2500;
            setq General_Auldsbel, 8;
            mesn;
            mesq l("Thanks. Snakes seems promising indeed! Maybe they work where scorpions failed. If I succeed, I promise I'll teach you the spell. But for now...");
            mesc l("Gained 20000 XP and 2500 GP");
        }
        break;
    // Learn transmigration: Snake Egg, Tongue, Skin
    case 8:
        if (MAGIC_LVL < 3)
            goto L_Magicless;
        mesn;
        mesq l("Well, I have not finished my research on snakes yet, but I'm pretty sure in how to transmute their parts.");
        next;
        setq General_Auldsbel, 9;
        mesn;
        mesq l("Here, look at how it is done. Focus. You can use it to convert a @@ into a @@, but not the other way around, for example.", getitemlink(MountainSnakeSkin), getitemlink(SnakeSkin));
        next;
        mesn;
        mesq l("Be careful as not everybody likes transmuted monster parts. Some may even see it as a foul thing. Anyway. Come back later.");
        close; // On purpose
        break;
    // Help on research to gain his favor
    case 9:
        mesn;
        // Obviously wrong, have you never read about butterflies?
        mesq l("So... I think I'm almost done! My plan is to force a @@ into being a pretty Butterfly. Hey, I like cute things!", getitemlink(SilkCocoon));
        next;
        mesn;
        mesq l("Besides, there are no Butterflies in Tulimshar, and they could help the farm in getting producing food.");
        next;
        mesn;
        mesq l("This time, I only a few last reagents and I'll finally attempt it... I want you to witness it. I'll teach you the spell later, of course.");
        mesc l("* @@/@@ @@", countitem(ManaPiouFeathers), 5, getitemlink(ManaPiouFeathers));
        mesc l("* @@/@@ @@", countitem(IceCube), 1, getitemlink(IceCube));
        mesc l("* @@/@@ @@", countitem(OceanCrocClaw), 1, getitemlink(OceanCrocClaw));
        next;
        select
            l("I'll try to find them."),
            l("I actually have them, here.");
        if (@menu == 2) {
            mes "";
            mesn;
            mesq l("Excellent! Let me see...");
            next;
            if (!transcheck(
                ManaPiouFeathers, 5,
                IceCube, 1,
                OceanCrocClaw, 1))
                    goto L_Lie;
            mesn;
            mesq l("Now lo and behold... The ultimate... TRANSMUTATION!");
            // Pray that you don't get disconnected now
            next;
            getexp 10000, 500;
            setq General_Auldsbel, 10;
            npctalk l("@@... I think something went wrong... RUN!!", strcharinfo(0));
            .@mob=monster(.map$, .x, .y, "Failed Experiment", GrassSnake, 1, .name$+"::OnSnakeDeath");
            specialeffect FX_MAGIC, SELF, getcharid(3);
            specialeffect FX_MGWARP, SELF, .name$;
            specialeffect FX_ATTACK, AREA, .@mob;
            // TODO: Maybe we should use unitattack()? Not need but...
            // We could also reconfigure this snake Mode to exclude ChangeTarget
            closeclientdialog;
            close;
        }
        break;
    // Learn Halhiss, completing Audsbel Quest
    case 10:
        // Actually, that's lv 60... Learning a lv 40 spell '-'
        if (MAGIC_LVL < 3)
            goto L_Magicless;
        if (mobcount(.map$, .name$+"::OnSnakeDeath")) {
            mesn;
            mesq l("Could you please dispose my failed experiment, first?");
            close;
        }
        mesn;
        mesq l("*sigh* Yet another failure... Transmutation and Nature Magic doesn't marry well.");
        next;
        skill(TMW2_HALHISS,1,0);
        setq General_Auldsbel, 12;
        mesn;
        mesq l("Next time, I'll ask Sagratha to help. Heh. I doubt that's going to happen. By the way, this is the @@ spell. It summons snakes...", b("@sk-halhiss"));
        next;
        mesn;
        mesq l("You need a @@ for it. Ah, back to research I guess...", getitemlink(SnakeEgg));
        next;
        break;
    default:
        mesn;
        mesq l("Uhm, no, not really. Maybe later, who knows?");
        break;
    }
    next;
    goto L_Main;


// Fallbacks
L_Magicless:
    mesn;
    mesq l("Well, you helped me. That's great! One hand washes the other, so, I'm willing to share knowledge with you.");
    next;
    mesn;
    mesq l("But unless you touch a Mana Stone and get stronger magic, that would be as useful as teaching magic to a wall. No offense.");
    next;
    mesn;
    mesq l("So, please, come to me with stronger magic powers. And then, I'll teach you a new magic spell.");
    close;

L_Lie:
    mesn;
    mesq l("Really interesting, how I am NOT seeing the items I asked for...");
    next;
    mesn;
    mesq l("Say, what do you think if I transmuted your head into the missing materials? I can warrant your soul won't return to the Soul Menhir, either!");
    close;

L_Lame:
    setparam(MaxHp, readparam(MaxHp)-50); // I want to see how permanent this is
    setparam(MaxSp, readparam(MaxSp)-25); // I want to see how permanent this is
    //setparam(Karma, readparam(Karma)-1); // testing
    mesn;
    mesq l("%%3 You sadden me. That was so, so lame. I will need to punish you. Sorry. Superior orders. %%S");
    next;
    mesn strcharinfo(0);
    mesq l("%%i What, my maximum life and mana just decreased! Noooo!!");
    next;
    mesn;
    mesq l("%%1 Cheer up, these should go back to normal when you level up. Just don't do that again!");
    // If that is true or not, remains to be checked. Uh... I never used setparam() before! :D
    close;

OnSnakeDeath:
    end;

OnInit:
    .sex=G_MALE;
    .distance=5;
    end;
}

