// TMW2 scripts.
// Authors:
//    Jesusalva
// Description:
//    Protect Nivalis
//    Q_NivalisLibday
//    Day, Score, Temporary Time;

019-2,44,54,0	script	Guard#019-2.1	NPC_GUARD1,{
    .@q=getq(Q_NivalisLibday);

    if ($NIVALIS_LIBDATE) {
        mesn;
        mesq l("Nivalis was liberated @@ ago.", FuzzyTime($NIVALIS_LIBDATE));
        close;
    } else if ($NLIB_DAY > 0) {
        if ($NLIB_DAY >= 7) goto L_MK;
        if (.@q == $NLIB_DAY) goto L_Delay;
        if (BaseLevel < 30) goto L_Noob;
        if (BaseLevel >= 30) goto L_Veteran;
        close;
    } else if (is_admin()) { // NOTE: This is an override label
        mesc "Initiate Nivalis Liberation Day?", 1;
        mesc "Event will last 7 days.", 1;
        if ($NLIB_SEQDAY)
            mesc "The event is currently set to happen in "+($NLIB_SEQDAY-gettimeparam(GETTIME_DAYOFMONTH))+" days.";
        else
            mesc b("Liberation Day is NOT scheduled to happen."), 1;
        select
            "Not yet.",
            "Yes",
            "No";
        if (@menu == 2) {
            closeclientdialog;
            goto OnNLibStart;
        }
        close;
    } else {
        legiontalk; end;
    }
    end;

OnNLibStart:
    addmapmask "019-3", MASK_MATTACK;
    $NLIB_DAY=1;
    $NLIB_HIGHTIME=0;
    $NLIB_HIGHNAME$="";
    setmapflagnosave("019-3", "000-1", 22, 22);
    setmapflagnosave("020-1", "000-1", 22, 22);
    setmapflagnosave("023-2", "000-1", 22, 22);
    setmapflag("019-3",mf_bexp,25);
    setmapflag("020-1",mf_bexp,150);
    disablenpc "#019-1_70_21";
    disablenpc "#019-2_37_55";
    disablenpc "#020-1_70_128";
    disablenpc "#020-1_107_55";
    enablenpc "Lightbringer#NLib";
    kamibroadcast("Nivalis Liberation Day has started.");
    $NLIB_SEQDAY=false;
    end;

// Event Selectors
L_Noob:
    mesn;
    mesq l("Hey, you! We need help to get rid from some remaining monsters at Nivalis City.");
    mesq l("No need to kill the Fluffies, though.");
    next;
    mesn;
    mesq l("Are you up for the challenge?");
    if (askyesno() == ASK_YES) {
        // Begin quest. Logout/Death will cause loss of the current day.
        setq1 Q_NivalisLibday, $NLIB_DAY;
        @nlib_wave=0;
        @nlib_time=300; // This makes sure that wave 1 will start.
        addtimer(5000, "#NLib_Siege::OnLoop");
        warp "020-1", rand(69,82), rand(78, 91);
    }
    close;

L_Veteran:
    mesn;
    mesq l("Hey, you! We need help to find the Monster King.");
    next;
    mesn;
    mesq l("Could you head deep in the woods and track him down?");
    if (askyesno() == ASK_YES) {
        // Control if you already found the Monster King
        @QNL3=0;
        // Begin quest. Logout/Death will cause loss of the current day.
        setq1 Q_NivalisLibday, $NLIB_DAY;
        setq3 Q_NivalisLibday, gettimetick(2);
        warp "019-3", any(128, 129, 130, 131, 132), any(24, 25, 26, 27);
        doevent("Guard#019-3.1::OnBegin");
        closedialog;
    }
    close;

L_MK:
    if (gettime(3) != 18 && !$@GM_OVERRIDE) {
        mesn;
        mesq l("Today at 18:30 UTC we are going to attack the Monster King by surprise. There will be no delays, so be there.");
    } else {
        mesn;
        mesq l("Do you want to go against the Monster King now? The event will start 18:30 UTC sharply.");
        if (askyesno() == ASK_YES) {
            // Control if you already found the Monster King
            @QNL3=0;
            // Begin quest. Logout/Death will cause loss of the current day.
            setq1 Q_NivalisLibday, $NLIB_DAY;
            setq3 Q_NivalisLibday, gettimetick(2);
            warp "019-3", any(128, 129, 130, 131, 132), any(24, 25, 26, 27);
            doevent("Guard#019-3.1::OnAdvise");
            closedialog;
        }
    }
    close;

// Misc
L_Delay:
    mesn;
    mesq l("You already helped us today. Come back tomorrow.");
    next;
    mesc l("Do you want to cross to the other side? You'll need to find the sea to return here if you do."), 1;
    next;
    if (askyesno() == ASK_YES)
        warp "019-1", 70, 30;
    close;

OnInit:
    if ($NLIB_DAY)
            addmapmask "019-3", MASK_MATTACK;

    .sex = G_MALE;
    .distance = 5;
    end;

OnHour00:
    if ($NLIB_DAY)
        $NLIB_DAY+=1;
    // Begin Nivalis Liberation Day
    if ($NLIB_SEQDAY == gettimeparam(GETTIME_DAYOFMONTH))
        goto OnNLibStart;
    end;

OnClock1800:
    if ($NLIB_DAY == 7) {
        kamibroadcast("All players, Nivalis Liberation Day starting in ##Bhalf hour##b.");
        kamibroadcast("Failing this event will change the game world FOREVER and in an IRREVERSIBLE way.");
    }
    end;

OnClock1825:
    if ($NLIB_DAY == 7)
        announce "All players, Nivalis Liberation Day starting in five minutes.", bc_all | bc_npc;
    end;

OnClock1830:
    if ($NLIB_DAY != 7)
        end;
    setmapflag("023-2",mf_bexp,200);
    setmapflag("023-2",mf_nopenalty);
    donpcevent("The Monster King#NLib::OnBegin");
    end;
}

