// TMW 2 Script
// Author:
//  Jesusalva
//  Micksha
// Description:
//  Controls Weapons Room basement door

042-1,51,23,0	script	#KDoor0421	NPC_HIDDEN,0,0,{
    end;

OnTouch:
    .@g=getcharid(2);
    if ($KAMELOT_KEYMASK[.@g] & 16) {
        warp "042-2@"+.@g, 44, 59;

        // Maybe event must be fired
        if (!($KAMELOT_QUEST[.@g] & 4)) {
            .@label$=instance_npcname("#KDoor0422", $@KAMELOT_ID[.@g])+"::OnArrival";
            deltimer .@label$;
            addtimer 5000, .@label$;
        }
    } else {
        dispbottom l("This door is locked.");
        //doevent instance_npcname(.name$)+"::OnKillMob"; // Double-check
    }
    end;

OnKillBoss:
    .@g=getcharid(2);
    dispbottom l("You found a key.");
    getitem TreasureKey, 1;
    $KAMELOT_KEYMASK[.@g]=$KAMELOT_KEYMASK[.@g]|16;
    // Player Reward for completing this stage
    getitem GuildCoin, min(1, $KAMELOT_MX[.@g]/20);
    getexp $KAMELOT_MX[.@g]*25, $KAMELOT_MX[.@g]*5;
    // Guild Reward for completing this stage
    .@ggp=300+$KAMELOT_MX[.@g]*4;
    .@gxp=$KAMELOT_MX[.@g]*5;
    $GUILD_BANK[.@g]+=.@ggp;
    guildgetexp(.@gxp); // 5xp per player average level (max 500/750)
    // Announce
    mapannounce getmap(), strcharinfo(0)+" has found the key for the door!", 0;
    // Guild Master Notification
    .@gm$=getguildmaster(.@g);
    if (!getcharid(3, .@gm$)) end;
    .@gma=getcharid(3, .@gm$);
    .@gmb=getcharid(0, .@gm$);
    if (!isloggedin(.@gma, .@gmb)) end;
    message .@gm$, strcharinfo(0)+" found the key: Guild GP +"+.@ggp+" Guild XP +"+.@gxp;
    end;

OnKillMob:
    .@label$=instance_npcname(.name$)+"::OnKillMob";
    // Oh noes! No player attached D:
    // This kill is meaningless, RESPAWN IT, RESPAWN IT
    if (!playerattached()) {
        //.@i=instance_id();
        .@m$=instance_mapname("042-1");
        monster .@m$, 27, 67, "Intruder", Troll, 1, .@label$;
        debugmes "Kamelot: Mob killed without player attached. Troll spawned.";
        end;
    }

    // Now we have a player attached, we can do all checks.
    // For example, if you're lame cheater
    .@g=getcharid(2);
    if (.@g < 1)
        die();

    // I also want to give you exp
    getexp $KAMELOT_MX[.@g]*3, $KAMELOT_MX[.@g];

    // And finally, check if you're still not done killing it.
    if (mobcount(getmap(), .@label$))
        end;

    // Configure the wave
    .@avg=$KAMELOT_MX[.@g];
    .@gcount=$KAMELOT_PC[.@g];
    .@m$=getmap();
    sleep2(800); // Give ~1 second before respawn

    // Maybe we should advance the wave
    if ($@KAMELOT_WAVE[.@g] == 3) {
        debugmes "Kamelot %d: Boss Spawn", .@g;
        initnpctimer;
        setd("$@GTEMP_"+getmap(), .@avg);
        .@mcount=.@gcount;
    } else if ($@KAMELOT_WAVE[.@g] == 2) {
        .@mcount=.@gcount*2;
        mapannounce getmap(), "Don't make me come there myself!! GET RID OF THEM ALREADY!", 0;
    } else if ($@KAMELOT_WAVE[.@g] == 1) {
        .@mcount=.@gcount*3/2;
        mapannounce getmap(), "Guards! What are you waiting for?? Arrest them!!", 0;
    } else if ($@KAMELOT_WAVE[.@g] == 0) {
        .@mcount=.@gcount;
        mapannounce getmap(), "Guards!! Attack the intruders!!!", 0;
    } else {
        end;
    }
    freeloop(true);
    for (.@i=0; .@i < .@gcount; .@i++) {
        .@mobId=any(CursedSoldier, CursedArcher); // 50-50 ratio
        .@mob=areamonster(.@m$, 21, 24, 59, 99, strmobinfo(1, .@mobId), .@mobId, 1, .@label$);
        // Reconfigure the monster
        setunitdata(.@mob, UDT_LEVEL, .@avg);
        setunitdata(.@mob, UDT_STR, 1+.@avg/5);
        setunitdata(.@mob, UDT_AGI, 1+.@avg/5);
        setunitdata(.@mob, UDT_VIT, 1+.@avg/5);
        setunitdata(.@mob, UDT_INT, 1+.@avg/5);
        setunitdata(.@mob, UDT_DEX, 1+.@avg/5);
        setunitdata(.@mob, UDT_LUK, 1+.@avg/5);
        setunitdata(.@mob, UDT_ADELAY, 1872);
        setunitdata(.@mob, UDT_ATKRANGE, (.@mobId == CursedArcher ? any(5,6,7) : any(1,1,2)));
        // Battle Status
        setunitdata(.@mob, UDT_MAXHP,    .@avg*30);
        setunitdata(.@mob, UDT_HP,       .@avg*30);
        setunitdata(.@mob, UDT_ATKMIN,   .@avg*35/10);
        setunitdata(.@mob, UDT_ATKMAX,   .@avg*55/10);
        setunitdata(.@mob, UDT_DEF,      1+.@avg*8/10);
        setunitdata(.@mob, UDT_MDEF,     1+.@avg*4/10);
        setunitdata(.@mob, UDT_HIT,      .@avg*35/10); // Advised: x3
        setunitdata(.@mob, UDT_FLEE,     .@avg*3); // Advised: x4
        // Critical calculation
        .@min=1;
        .@max=max(.@min, min(20, .@avg/5));
        setunitdata(.@mob, UDT_CRIT,     rand2(.@min, .@max));
        // Loop through
    }
    freeloop(false);
    $@KAMELOT_WAVE[.@g]+=1;
    end;

OnTimer100:
    .@m$=instance_mapname("042-1");
    mapannounce .@m$, "*sigh* You force me to come...", 0;
    end;

OnTimer5000:
    .@m$=instance_mapname("042-1");
    mapannounce .@m$, "Stupid fools...", 0;
    end;

OnTimer10000:
    .@label$=instance_npcname(.name$)+"::OnKillBoss";
    .@m$=instance_mapname("042-1");
    mapannounce .@m$, "I'll get rid of you myself!!", 0;
    .@mobId=any(CursedSoldier, CursedArcher);
    .@mob=monster(.@m$, 40, 30, any("Lancelot", "Galahard", "Gareth", "Percival"), .@mobId, 1, .@label$);
    .@avg=getd("$@GTEMP_"+.@m$);
    setd("$@GTEMP_"+.@m$, 0);
    // Reconfigure the monster
    setunitdata(.@mob, UDT_LEVEL, .@avg);
    setunitdata(.@mob, UDT_STR, 1+.@avg/5);
    setunitdata(.@mob, UDT_AGI, 1+.@avg/5);
    setunitdata(.@mob, UDT_VIT, 1+.@avg/5);
    setunitdata(.@mob, UDT_INT, 1+.@avg/5);
    setunitdata(.@mob, UDT_DEX, 1+.@avg/5);
    setunitdata(.@mob, UDT_LUK, 1+.@avg/5);
    setunitdata(.@mob, UDT_ADELAY, 1672);
    setunitdata(.@mob, UDT_ATKRANGE, (.@mobId == CursedArcher ? any(6,7,8) : 2));
    // Battle Status
    setunitdata(.@mob, UDT_MAXHP,    .@avg*150);
    setunitdata(.@mob, UDT_HP,       .@avg*150);
    setunitdata(.@mob, UDT_ATKMIN,   .@avg*5);
    setunitdata(.@mob, UDT_ATKMAX,   .@avg*7);
    setunitdata(.@mob, UDT_DEF,      1+.@avg*11/10);
    setunitdata(.@mob, UDT_MDEF,     1+.@avg*6/10);
    setunitdata(.@mob, UDT_HIT,      .@avg*8); // Advised: x3
    setunitdata(.@mob, UDT_FLEE,     .@avg*35/10); // Advised: x4
    // Critical calculation
    .@min=15;
    .@max=max(.@min, min(40, .@avg/5));
    setunitdata(.@mob, UDT_CRIT,     rand2(.@min, .@max));
    stopnpctimer;
    end;

OnArrival:
    .@g=getcharid(2);
    if (.@g < 1)
        die();
    if (getmap() != "042-1@"+.@g)
        end;
    if ($@KAMELOT_WAVE[.@g] == 0)
        goto OnKillMob;
    end;
}


// Required exit
042-1,55,100,0	script	#KDoor0421B	NPC_HIDDEN,0,0,{
    end;

OnTouch:
    .@g=getcharid(2);
    if ($KAMELOT_KEYMASK[.@g] & 16) {
        dispbottom l("WARNING: If you walk out the main gate you WON'T be able to return!");
        warp "042-0@"+.@g, 69, 25;
    } else {
        dispbottom l("Oh noes! The guards locked the door!");
    }
    end;
}

