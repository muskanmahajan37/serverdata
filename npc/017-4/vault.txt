// TMW2/LoF Script.
// Author:
//    Jesusalva
// Notes:
//    Based on BenB idea.

017-4,20,41,0	script	Vault#0174	NPC_VAULT,{
    LootableVault(2, 5, "0174");
    close;

OnInit:
    .distance=3;
    end;

OnClock0201:
OnClock1418:
    $VAULT_0174+=rand2(27,40);
    end;
}

