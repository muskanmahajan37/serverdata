// TMW2 scripts.
// Authors:
//    Jesusalva
// Description:
//    Allows to mount your own fish/plushroom/croconut box

-	script	Empty Box#it	NPC_HIDDEN,{
    close;

OnUse:
    mesn;
    mesc l("You can fill this box with the following:");
    mesc l("- @@ @@", 7, getitemlink(GrassCarp));
    mesc l("- @@ @@", 8, getitemlink(Croconut));
    mesc l("- @@ @@", 20, getitemlink(CommonCarp));
    mesc l("- @@ @@", 87, getitemlink(Plushroom));
    mes "";
    mesc l("Fill with what?");
    select
        l("Don't fill"),
        rif(countitem(GrassCarp) >= 7, l("7 Grass Carp")),
        rif(countitem(Croconut) >= 8, l("8 Croconut")),
        rif(countitem(CommonCarp) >= 20, l("20 Common Carp")),
        rif(countitem(Plushroom) >= 87, l("87 Plushroom"));
    mes "";
    switch (@menu) {
        case 2:
            delitem GrassCarp, 7;
            getitem FishBox, 1;
            break;
        case 3:
            delitem Croconut, 8;
            getitem CroconutBox, 1;
            break;
        case 4:
            delitem CommonCarp, 20;
            getitem FishBox, 1;
            break;
        case 5:
            delitem Plushroom, 87;
            getitem PlushroomBox, 1;
            break;
        default:
            getitem EmptyBox, 1;
    }
    delitem EmptyBox, 1;
    closedialog;
    close;
}
