// TMW2 scripts.
// Authors:
//    Jesusalva
// Description:
//    Frostia King Guards

024-16,33,42,0	script	Royal Guard#02416A	NPC_BRGUARD_SPEAR,{
    legiontalk;
    end;

OnInit:
    .distance=5;
    end;
}

024-16,25,33,0	duplicate(Royal Guard#02416A)	Royal Guard#02416B	NPC_BRGUARD_SWORD
024-16,35,33,0	duplicate(Royal Guard#02416A)	Royal Guard#02416C	NPC_BRGUARD_BOW


// Before King Gelid give you his OK, you cannot leave throne room
024-16,30,53,0	script	#FrostiaKingAudience	NPC_HIDDEN,1,0,{
end;
OnTouch:
    if (getq(General_Narrator) <= 12) {
        slide 30, 52;
        dispbottom l("Ops, I should not leave this room without talking to the king first.");
    }
    end;
}

