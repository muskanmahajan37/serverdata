// TMW2 Script
// Author:
//  Jesusalva
// Description:
//  Smith System (Player, Guild, NPC)
// Notes:
//  It's like smithing, but it only change an item options

// Usage: SmithTweakReset ()
// Asks if player wants to remove an item options. And remove them.
function	script	SmithTweakReset	{
    mesc l("Note: This action cannot be undone."), 1;
    mes l("Drag and drop here the item you want to remove the options.");

    .@id=requestitemindex();
    mes "";

    // Ask player to confirm
    mesc l("Are you sure?"), 1;
    mesc l("Note: This action cannot be undone."), 1;
    if (!csys_Confirm(.@id))
        return;

    csys_Check(.@id);
    return;
}

// Usage: SmithTweakSystem ({price=600, retries=1})
// Returns true on success, false on failure
function	script	SmithTweakSystem	{
    .@price=getarg(0, 600);
    .@retry=getarg(1, 1);

	// Adjust price (if relevant)
    if (.@retry == 1)
    	.@price=POL_AdjustPrice(.@price);

    // How many times more can you tweak?
    // You get 1 action, capped to 6
    .@left=gettimeparam(GETTIME_HOUR)-SMITH_TWEAKS;
    if (.@left > 6) {
        .@left=6;
        SMITH_TWEAKS=gettimeparam(GETTIME_HOUR)-6;
    }

    mes l("Which item will you tweak?");
    mesc l("Note: You can only perform this operation @@/6 times.", .@left);
    mesc l("You recover a tweaking point every hour.");
    mesc l("EXPERTS ONLY - If you are not a talented crafter, avoid this."), 1;
    mesc l("The item must have a previous bonus, which WILL BE LOST!"), 1;
    mesc l("Note: You may fail to write skills to it."), 1;
    mesc l("Operation Cost: @@ GP", .@price), 3;

    // Do you have money or AP
    if (Zeny < .@price || !.@left) {
        mesc l("You lack money or Action Points."), 1;
        return false;
    }

    .@id=requestitemindex();
    mes "";

    // Ask player to confirm
    if (!csys_Confirm(.@id))
        return false;

    // Collect the item ID
    delinventorylist();
    getinventorylist();
    .@x=@inventorylist_id[.@id];

    // No duplicates
    if (countitem(.@x) > 1) {
        mesc l("You are carrying duplicates of the same item. Sorry, but I have no idea which one you want to tweak."), 1;
        return false;
    }

    // Skip equipped items
    if (isequipped(.@x)) {
        mesc l("You should unequip this item first."), 1;
        return false;
    }

    // If the item have no bonuses - fail
    setarray .@AlwaysTweaks, 65535, BlacksmithAxe, Dustynator, Lightbringer,
                             DemureAxe, Tyranny, Runestaff, AegisShield,
                             SaviorShield, SaviorArmor, SaviorBoots, SaviorPants,
                             Skypiercer;

    // Tweaked items
    if (getitemoptionidbyindex(.@id, 0) <= 0 && !is_master() && array_find(.@AlwaysTweaks, .@x) < 0) {
        mesc l("This item have no bonuses, and cannot be tweaked."), 1;
        return false;
    }

    // Take the money and AP away
	POL_PlayerMoney(.@price);
    if (.@x != Lightbringer)
        SMITH_TWEAKS+=1;

    // Apply the bonuses. This will only loop if `continue;` is cast.
    // `continue` will only be cast if .@retry is set
    do
    {
        .@retry-=1;
        // Check if you fail
        if (!csys_Check(.@id)) {
            mesc l("YOU FAIL! It is a simple item now."), 1;
            if (.@retry) {
                mesc l("...Automatically retrying...");
                continue;
            }
            return false;
        }

        csys_Apply(.@id);
        mesc l("SUCCESS! Congratulations, the item was improved!"), 3;
        if (.@retry) {
            next;
            mesc l("Do you want to re-roll?"), 1;
            if (askyesno() == ASK_YES) {
                continue;
            }
        }
        return true;
    } while (.@retry > 0);
}
