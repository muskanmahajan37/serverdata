// TMW2 Scripts
// Author:
//    Jesusalva
// Description:
//    Random NPC (HF = Human Female)

027-1,132,112,0	script	Student#A01	NPC_ACADEMY_HF,{

studenttalk();
close;

OnTimer1000:
    domovestep();
    end;

OnInit:
    initpath "move", 132, 112,
             "dir", DOWN, 0,
             "wait", 20, 0,
             "move", 132, 122,
             "dir", DOWN, 0,
             "wait", 20, 0,
             "dir", UP, 0,
             "wait", 1, 0,
             "move", 132, 102,
             "dir", DOWN, 0,
             "wait", 20, 0,
             "dir", LEFT, 0,
             "wait", 1, 0,
             "move", 115, 102,
             "dir", DOWN, 0,
             "wait", 20, 0,
             "move", 112, 112,
             "dir", DOWN, 0,
             "wait", 20, 0,
             "dir", RIGHT, 0,
             "wait", 2, 0;

    initialmove;
    initnpctimer;

    .sex = G_FEMALE;
    .distance = 5;
    end;
}
