// TMW 2 script
// Author:
//    Saulc

005-2,24,36,0	script	Trap Door	NPC_HIDDEN,0,0,{
    end;

OnTouch:
    .@chest = getq(CandorQuest_Chest);
    if (.@chest == 1)
        warp "005-2-1", 28, 35;
    close;
}
