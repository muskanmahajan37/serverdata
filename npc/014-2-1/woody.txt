// TMW2 Script
// Author:
//      Saulc
//      Jesusalva
// Description:
//    Woody, is the oldest player of TMW2, he deserves his own npc :d an old trapper who give help to new player to craft Squirrel boots
// id:100  HurnscaldQuest_Woody
//     <2 - See his wife
//      3 - Access to House
//      4 - Quest Accepted
//      5 - Quest Complete

014-2-1,37,43,4	script	Woody	NPC_PLAYER,{
    .@Woo = getq(HurnscaldQuest_Woody);
    if (BaseLevel < 26) goto L_TooWeak;
    if (.@Woo == 4) goto L_Check;
    if (.@Woo == 5) goto L_Complete;

L_GiveTask:
    mesn;
    mesq l("Hi, I'm Woody!");
    next;
    mesq l("Do you need something in particular?");
    next;

    menu
        l("To be honest, I don't know. Your wife just allowed me to pass the door!"), L_Quit,
        l("As I know, you're kind of hunter crafter, Right?"), L_Quest,
        l("Sorry, It's not place for me."), L_Quit;


L_Quest:
    mes "";
    mesn;
    mesq l("Ah yes... I make fine @@...", getitemlink(SquirrelBoots));
    next;
    mesn;
    mesq l("If you bring me some items, I can make some for you!");
    next;

    menu
        l("Really? What do you need?"), L_Start,
        l("Better do this some other time..."), L_Quit;


L_Start:
    setq HurnscaldQuest_Woody, 4;
    mes "";
    mesn;
    mesq l("Ok, what I need is:");
    goto L_List;

L_Quit:
    mes "";
    mesn;
    mesq l("Alright.");
    close;

L_List:
    mes "";
    mesn;
    mes l("Here's what I need:");
    mes l("@@/50 @@", countitem(SquirrelPelt), getitemlink(SquirrelPelt));
    mes l("@@/10 @@", countitem(CottonCloth), getitemlink(CottonCloth));
    mes l("@@/1 @@", countitem(Boots), getitemlink(Boots));
    close;

L_Check:
    mesn;
    mesq l("Did you brought me everything I asked for?");
    mesq l("Remember, I'll make a @@ for you!", getitemlink(SquirrelBoots));
    next;
    menu
        l("Yes!"), L_Give,
        l("I forgot what you need!"), L_List,
        l("No!"), L_Quit;

L_Give:
    if (
        countitem(Boots)                < 1  ||
        countitem(CottonCloth)          < 10 ||
        countitem(SquirrelPelt)         < 50
        ) goto L_Lying;

    inventoryplace SquirrelBoots, 1;

    delitem(Boots, 1);
    delitem(CottonCloth, 10);
    delitem(SquirrelPelt, 50);

    getitem(SquirrelBoots, 1);
    getexp(3000, 20);
    setq(HurnscaldQuest_Woody, 5);

    mes "";
    mesn;
    mesq l("Here, all yours. Ah, if I still could walk... I would hunt those annoying squirrels myself!");
    close;

L_Complete:
    mesn;
    mesq l("Wandering too much? Take care to don't get lost.");
    close;

L_Lying:
    mesn;
    mesq l("No no no, that's wrong.");
    next;
    mesn;
    mesq l("You don't have everything I've asked for!");
    next;
    goto L_List;

L_TooWeak:
    mesn;
    mesq l("Hey kid, go play somewhere else, I don't have time! It's soon over!");
    close;

OnInit:
    .@npcId = getnpcid(.name$);
    setunitdata(.@npcId, UDT_HEADTOP, MinerHat);
    setunitdata(.@npcId, UDT_HEADMIDDLE, LeatherShirt);
    setunitdata(.@npcId, UDT_HEADBOTTOM, BromenalPants);
    setunitdata(.@npcId, UDT_WEAPON, DeepBlackBoots);
    setunitdata(.@npcId, UDT_HAIRSTYLE, 4);
    setunitdata(.@npcId, UDT_HAIRCOLOR, 3);

    npcsit;
    .sex = G_MALE;
    .distance = 4;

    end;
}

