// TMW2/LoF scripts.
// Authors:
//    Jesusalva
// Description:
//    Part of player story

017-10,57,34,0	script	Kenton	NPC_KENTON,{
    .@q=getq(General_Narrator);
    mesn;
    mesq l("Hello! My name is Kenton, and I'm in charge of Public Affairs of Land Of Fire.");
    next;
    mesn;
    mesq l("You see, Pihro and Pyndragon, the mayors, are really busy people. They're developing a game or something as we speak.");
    next;
    mesn;
    mesq l("So if you have any issue which would require the Mayor to see it, you'll need me first.");
    switch (.@q) {
        case 0:
            Exception("ERROR", RB_SPEECH|RB_ISFATAL);
        case 17:
            next;
            mesc l("@@ raise an eyebrow as you hand him Gelid's letter.", .name$);
            next;
            mesn;
            mesq l("That's a really strange letter from Mr. Frozenheart.");
            next;
            mesn;
            mesq l("But also very amusing. I'm sure Pihro and Pyndragon, the town mayors, will fancy this request of yours.");
            next;
            mesn;
            mesq l("Anyway, as you might be aware, Land Of Fire came here in an incident after the Monster King took all Mana Stones... So, we have a good magical affinity.");
            next;
            mesn;
            mesq l("Most of our professors moved on to the Academy Island, and are with Tulimshar's and Frostia's professors working right now. But...");
            next;
            mesn;
            mesq l("...Lalica, the witch, is still with us. And she came to complain that a petty thief stolen a very precious item of hers.");
            next;
            mesn;
            mesq l("I don't know what, but it is ")+b(l("small, red, round and shiny."))+l("And we know who the thief is.");
            next;
            mesn;
            mesq l("I've dispatched Benjamin and some other city guards after her flew to Elenium Mines, but thus far, they haven't returned.");
            next;
            mesn;
            mesq l("I need you to find her to and bring her back there, along whatever she stole. Then I'll honor your request.");
            next;
            mesn;
            mesq l("After all, that was two nights ago, and none of them returned...");
            mesc b(l("WARNING: "))+l("Minimum advised level 72 to do this quest."), 1;
            mesc b(l("WARNING: "))+l("You would do well as come prepared, as failing in the last stage will RESET current progress on the quest."), 1;
            setq LoFQuest_Barbara, 1;
            setq General_Narrator, 18;
            break;
        // Barbara Quest in progress
        case 18:
            .@s=getq(LoFQuest_Barbara);
            .@s3=getq3(LoFQuest_Barbara);
            switch (.@s) {
            case 5:
                // No apple - you fail
                if (!.@s3) {
                    mesn;
                    mesq l("Good luck arresting the criminal!");
                    mesc l("You need both the stolen item as the thief to complete the quest."), 1;
                    close;
                }

                // Good reporting
                mesn;
                if (!BARBARA_STATE)
                    mesq l("I see you've brought Barbara here, excellent. She seems pretty beat up.");
                else
                    mesq l("...Where's Barbara, the thief?");
                next;
                mesn;
                mesq l("Also, do you have the stolen item?");
                // You fail
                if (!(askyesno() == ASK_YES && countitem(MagicApple))) {
                    mes "";
                    mesn;
                    mesq l("Bring me the stolen item, pretty please.");
                    close;
                }
                mes "";
                mesn;
                mesq l("Good, there they are.");
                next;
                if (BARBARA_STATE) {
                    mesn strcharinfo(0);
                    mesq l("I regret to inform Barbara was killed in battle before I could find her. I burried her in the mines.");
                    next;
                    mesn;
                    mesq l("Oh... What a bummer. But you've brought the item back, so we're OK.");
                    next;
                // Barbara is alive
                } else {
                    mesc l("Pledge for Barbara's innocence?");
                    // Pledge menu
                    if (askyesno() == ASK_YES) {
                        BARBARA_STATE=3;
                        mesn;
                        mesq lg("...That's a surprise, the noble @@ defending a criminal like her.", strcharinfo(0));
                        next;
                        mesn;
                        mesq l("I will let Lalica, Pihro and Pyndragon know that you find her not guilty, but...");
                        next;
                        mesn;
                        mesq l("The final sentence still belongs to LoF Admins.");
                        next;
                    } else {
                        BARBARA_STATE=2;
                    }
                    // EOF: Pledge Menu
                    mesn;
                    mesq l("I'll have her hospitalized now.");
                    next;
                }
                mesn;
                mesq l("Anyway, here is your reward.");
                delitem MagicApple, 1;
                setq General_Narrator, 19;
                setq LoFQuest_Barbara, 0, 0, 0;
                getexp 400000, 15000; // Needed: 1,148,484
                Zeny+=3300; // You get a reward in cash
                next;
                mesn;
                mesq l("Please come back later to know how things are going.");
                break;
            // Bad reporting
            case 4:
                mesn;
                mesq l("Good luck arresting the criminal!");
                mesc l("You need both the stolen item as the thief to complete the quest."), 1;
                break;
            // Cutscene not seen
            case 1:
                mesn;
                mesq l("Good luck arresting the criminal!");
                break;
            // Default message
            default:
                mesn;
                mesq l("Benjamin reported in. Good luck arresting the criminal!");
                break;
            }
            break;
        // Barbara Quest was complete and solution was marked in BARBARA_STATE
        case 19:
            next;
            mesn;
            mesq l("So, where we were again... Oh right, travel to world edge, look for family or whatever, right?");
            next;
            mesn;
            mesq l("Yes. I mean, yes, Pihro and Pyndragon will fancy you an airship.");
            next;
            select
                l("What is an airship? O.o"),
                l("Cool, an airship!");
            mes "";
            if (@menu == 1) {
                mesn;
                mesq l("An Airship? It is just a ship. Which flies.");
                next;
            }
            mesn;
            mesq l("Now, crafting an airship requires a lot of efforts, resources, money and specially time. Fourteen months, to be exact.");
            next;
            mesn;
            mesq l("By the way , it is done already. You took a long time, you know? Also, we're talking about Pihro and Pyndragon.");
            next;
            mesn;
            mesq l("Besides, the Alliance High Council had plans for an expedition on the Fortress Island, but had no personel to do it.");
            next;
            mesn;
            mesq l("Yes, you are now personel! Doesn't it feels exciting? Please tell me it does, I have nothing else to convince you to go otherwise.");
            next;
            mesn;
            mesq l("The Fortress Island is a terribly dangerous place, so the Alliance sent a scout party beforehand, and set up a tower in front of an... erm... Very disturbing walled place we found.");
            next;
            mesn;
            mesq l("Now go and save the world or something like that. I mean, it is not like I really cared with the world, anyway... With luck you'll even find what you are looking for.");
            setq General_Narrator, 20;
            // FALLTHROUGH
    case 20:
    case 21:
            next;
            setcamnpc "#ToFortress";
            mesn;
            mesq l("Just go over there and you'll be able to board the Airship. The travel takes a while so please be patient and good luck!");
            restorecam;
            // TODO: Maybe investigate & report to Tulimshar Magic Council?
            break;
    }
    close;

OnInit:
    .sex=G_MALE;
    .distance=5;
    end;

}

