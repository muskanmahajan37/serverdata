// TMW-2 Script
// Author:
//    Jesusalva
// Description:
//    Nicholas is Hurnscald's blacksmith. He forges some stuff, and sell other stuff.
//    Perhaps he should not forge armor? Remember he cannot forge EVERYTHING...
//
// PS.
// Iridium + Platyna        Platinum
// Gold + Coal              Gold
// Silver + Coal            Silver
// Copper + Tin             Bronze (9:1)
// Terranite + Coal         Terranite
// Iron + Coal              Iron

// Resitance and Weight increasing ores
// Lead → +++ res, ++++ wei
// Titanium → + res, + wei

// Originals: Setzer, Steel Shield, Chain Mail, Light Platemail, Warlord Plate, Warlord Boots

012-5,36,26,0	script	Nicholas	NPC_NICHOLAS,{
    goto L_Menu;

    // blacksmith_header()
    function blacksmith_header {
        mesn;
        mesq l("Very well! We have seven class of items: Wood, Iron, Terranite, Bronze, Silver, Gold and Platinum.");
        mesq l("Each of them require different items, I'll sort from weakest to strongest, so choose wisely.");
        return;
    }

    // blacksmith_create( BaseItem1, Amount, BaseItem2, Amount, PrizeItem, Price )
    function blacksmith_create {
        .@base1=getarg(0);
        .@amon1=getarg(1);
        .@base2=getarg(2);
        .@amon2=getarg(3);
        .@prize=getarg(4);
        .@price=getarg(5);

		// Adjust price
		.@price=POL_AdjustPrice(.@price);

        mesn;
        mesq l("Do you want to craft @@? For that I will need:", getitemlink(.@prize));
        mesc l("@@/@@ @@", countitem(.@base1), .@amon1, getitemlink(.@base1));
        mesc l("@@/@@ @@", countitem(.@base2), .@amon2, getitemlink(.@base2));
        mesc l("@@/@@ GP", format_number(Zeny), format_number(.@price));

        select
            l("Yes"),
            l("No");

        if (@menu == 2)
            return;

        if (countitem(.@base1) >= .@amon1 &&
            countitem(.@base2) >= .@amon2 &&
            Zeny >= .@price) {
            inventoryplace .@prize, 1;
            delitem .@base1, .@amon1;
            delitem .@base2, .@amon2;
			POL_PlayerMoney(.@price);

            // craft the item with +30% crit dmg (weapons) or +1 random stat (etc)
            if (array_find(.WeaponCraft, .@prize) >= 0)
                CsysNpcCraft(.@prize, IOPT_CRITDMG, rand(28,32));
            else
                CsysNpcCraft(.@prize, any(0, VAR_STRAMOUNT, VAR_AGIAMOUNT, VAR_VITAMOUNT, VAR_INTAMOUNT, VAR_DEXAMOUNT, VAR_LUKAMOUNT), any(1,1,1,2));

            // Yield some experience for the craft
            .@xp=getiteminfo(.@base1, ITEMINFO_SELLPRICE)*.@amon1+getiteminfo(.@base2, ITEMINFO_SELLPRICE)*.@amon2;
            .@xp=.@xp*2/3;
            getexp .@xp, rand(1,10);

            mes "";
            mesn;
            mesq l("Many thanks! Come back soon.");
        } else {
            speech S_FIRST_BLANK_LINE,// | S_LAST_NEXT,
                    l("You don't have enough material, sorry.");
        }
        return;
    }

L_Menu:
    mesn;
    mesq l("Hello there, I am @@, blacksmith of this fine city. What do you want to forge today?", .name$);
    mes "";
    select
        l("I just want to trade."),
        l("I want to forge One Handed Weapons!"),
        l("I want to forge Shields!"),
        l("I want to forge Quivers!"),
        rif(is_staff() && getskilllv(TMW2_CRAFT), l("I would like to remove an item options")),
        rif(getskilllv(TMW2_CRAFT), l("I would like to change an item options")),
        l("Nothing, thanks!");

    mes "";
    switch (@menu) {
    case 1:
        npcshopattach(.name$);
        openshop;
        closedialog;
        close;
        break;
    case 2:
        goto L_Weapon;
    case 3:
        goto L_Shield;
    case 4:
        goto L_Quiver;
    case 5:
        SmithTweakReset();
        goto L_Menu;
    case 6:
        mesn;
        mesq l("You'll be charged even if you fail, be warned.");
        next;
        if (!SmithTweakSystem()) {
            mes "";
            mesn;
            mesq l("You can always try again another day!");
        }
        next;
        goto L_Menu;
    }
    close;










L_Weapon:
    blacksmith_header();
    select
        l("Nothing, sorry!"),
        l("Wooden Sword"),
        l("Iron Bug Slayer"),
        l("Iron Short Gladius"),
        rif(countitem(BronzeGladius), l("Bronze Gladius")),
        l("Iron Backsword");

    switch (@menu) {
    case 1:
        goto L_Menu;
    case 2:
        blacksmith_create(WoodenLog, 20, RawLog, 5, WoodenSword, 450);
        break;
    case 3:
        blacksmith_create(IronIngot, 8, Coal, 12, BugSlayer, 1000);
        break;
    case 4:
        blacksmith_create(IronIngot, 12, Coal, 16, ShortGladius, 1550);
        break;
    case 5:
        blacksmith_create(CopperIngot, 18, TinIngot, 2, RealBronzeGladius, 500);
        break;
    case 6:
        blacksmith_create(IronIngot, 18, TinIngot, 4, Backsword, 4550);
        break;
    }
    goto L_Weapon;






L_Shield:
    blacksmith_header();
    select
        l("Nothing, sorry!"),
        l("Wooden Shield"),
        l("Iron Blade Shield"),
        rif(getq(NivalisQuest_Baktar) >= 3, l("Bronze Braknar Shield"));

    switch (@menu) {
    case 1:
        goto L_Menu;
    case 2:
        blacksmith_create(WoodenLog, 40, LeatherPatch, 2, WoodenShield, 500);
        break;
    case 3:
        blacksmith_create(IronIngot, 14, TinIngot, 4, BladeShield, 1500);
        break;
    case 4:
        blacksmith_create(CopperIngot, 18, TinIngot, 2, BraknarShield, 8000);
        break;
    }
    goto L_Shield;






L_Quiver:
    blacksmith_header();
    select
        l("Nothing, sorry!"),
        l("Leather Quiver"),
        l("Iron Quiver"),
        l("Bronze Quiver"),
        l("Platinum Quiver");

    switch (@menu) {
    case 1:
        goto L_Menu;
    case 2:
        blacksmith_create(LeatherPatch, 35, CottonCloth, 5, LeatherQuiver, 2000);
        break;
    case 3:
        blacksmith_create(IronIngot, 16, Coal, 21, IronQuiver, 3000);
        break;
    case 4:
        blacksmith_create(CopperIngot, 27, TinIngot, 3, BronzeQuiver, 4000);
        break;
    case 5:
        blacksmith_create(PlatinumIngot, 12, IridiumIngot, 5, PlatinumQuiver, 50000);
        break;
    }
    goto L_Quiver;








OnInit:
	tradertype(NST_MARKET);
    sellitem Dagger, 600, 5;
    sellitem SharpKnife, 450, 10;

    .sex = G_MALE;
    .distance = 5;
    setarray .WeaponsCraft, WoodenSword, BugSlayer, ShortGladius, RealBronzeGladius, Backsword;
    end;

OnClock0009:
OnClock0603:
OnClock1207:
OnClock1801:
    restoreshopitem Dagger, 600, 5;
    restoreshopitem SharpKnife, 450, 10;
    end;
// Pay your taxes!
OnBuyItem:
    PurchaseTaxes("Hurns");
    end;

OnSellItem:
    debugmes("Sale confirmed");
    SaleTaxes("Hurns");
    end;
}
