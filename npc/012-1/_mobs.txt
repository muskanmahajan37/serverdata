// This file is generated automatically. All manually added changes will be removed when running the Converter.
// Map 012-1: Hurnscald mobs
012-1,90,63,35,14	monster	Red Butterfly	1025,4,30000,90000
012-1,90,62,36,15	monster	Mana Bug	1075,5,30000,90000
012-1,85,33,35,15	monster	Pinkie	1132,8,30000,20000,Hinnak::OnKillPinkie
012-1,44,56,21,41	monster	Clover Patch	1028,3,60000,90000
012-1,135,58,7,29	monster	Piousse	1003,6,40000,60000
012-1,85,86,52,15	monster	Silk Worm	1034,5,30000,25000
012-1,115,69,1,1	monster	Training Dummy	1021,1,10000,10000
012-1,81,59,54,35	monster	Squirrel	1032,12,30000,45000
