// TMW2 Script
// Author:
//    Jesusalva
// Description:
//    Hinnak's Wife, Terezin (renamed to make an Easter Egg)
// NPC_MONA?

012-1,110,41,0	script	Tezzerin	NPC_ELVEN_FEMALE_READING,{
    .@q1=getq(HurnscaldQuest_Farmers);
    .@q2=getq2(HurnscaldQuest_Farmers);
    if (strcharinfo(0) == $MOST_HEROIC$) mesn;
    if (strcharinfo(0) == $MOST_HEROIC$) mesq l("Ah, @@, welcome!", $MOST_HEROIC$);
    if (strcharinfo(0) == $MOST_HEROIC$) next;
    if (.@q1 == 5 && .@q2 == 0) goto L_PinkieHat;
    if (.@q1 == 4) goto L_Almost;
    if (.@q1 >= 2) goto L_Thanks;
    mesn;
    mesq l("Ah, my husband Hinnak is so hard working...");
    close;

L_Almost:
    mesn;
    mesq l("You deserve a reward for helping my husband Hinnak, but please, get Oscar's reward first.");
    close;

L_Thanks:
    mesn;
    mesq l("Thanks for helping my husband. I'll think if there's something I can do for you, why don't you help Oscar on the meanwhile...?");
    close;

L_PinkieHat:
    mesn;
    mesq l("You deserve a reward for helping my husband. I can make you a nice gift.");
    next;
    mesn;
    mesq l("Please bring me a single @@ and @@ GP, and I'll give you a memeto.", getitemlink(PinkAntenna), 6500);
    if (countitem(PinkAntenna) < 1 || Zeny < 6500)
        close;
    next;
    mesn strcharinfo(0);
    mesc l("Give the Antenna and the GP to her?");
    if (askyesno() != ASK_YES) {
        mesq l("I'll seek one for you.");
        close;
    }
    delitem PinkAntenna, 1;
    if (Zeny < 6500)
        close;
    Zeny=Zeny-6500;
    getitem PinkieHat, 1;
    mesn;
    mesq l("Here, I did this @@ for you! I'm pretty sure this is also a drop, but that's literally everything I could do for helping my husband.", getitemlink(PinkieHat));
    setq2 HurnscaldQuest_Farmers, 1;
    close;

OnInit:
    .sex = G_FEMALE;
    .distance = 5;
    end;
}

