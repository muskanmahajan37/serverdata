// TMW-2 Script
// Originals: TMW Org.
//
// Author:
//    Jesusalva
// Description:
//    Jack is Hurnscald Lumberjack

012-1,69,93,0	script	Jack	NPC_LUMBERJACK,{
    .@q1=getq(HurnscaldQuest_ForestBow);
    //.@q2=getq(HurnscaldQuest_WoodShield);

    mesn "Jack, the Lumberjack";
    mesq l("Hello there! I'm Jack, the Lumberjack, and enemy of all trees. If you need some firewood, just let me know.");
    mes "";
    select
        l("Nothing at the moment, thanks."),
        l("I want you to transform my raw log in a wooden log"),
        rif(.@q1 == 1, l("I need a sturdy wood piece for a Forest Bow."));

    mes "";

    switch (@menu) {
        case 1:
            close;
            break;
        case 2:
            goto L_Polish;
            break;
        case 3:
            goto L_ForestBow;
            break;
    }

    close;

L_Polish:
    mesn;
    mes l("Yes, unless you're doing something VERY special, a Wooden Log is what you need.");
    mes l("I can make a @@ for just one @@ and @@ GP for my work.", getitemlink(WoodenLog), getitemlink(RawLog), .cost);
    input .@count;
    mes "";

    if (.@count == 0)
        close;

    .@Cost = .@count * .cost;
    .@empty = countitem(RawLog);

    if (.@empty < .@count || Zeny < .@Cost) {
        mesn;
        mesq l("You can't afford my services for that amount. Sorry.");
    }

    inventoryplace WoodenLog, .@count;

    Zeny = Zeny - .@Cost;
    delitem RawLog, .@count;
    getitem WoodenLog, .@count;
    mesc l("There you go, %s. If you don't know anything about crafting I heard someone on Tulimshar's Terranite Forge can help?", lg("gal", "pal"));
    close;

L_ForestBow:
    .@k=getq2(HurnscaldQuest_ForestBow);

    switch (.@k) {
    case 0:
        mesn;
        mesq l("Ah, don't bother me with that. That wood must be special, it must bend and cannot break. Too difficult to find!");
        next;
        mesn;
        mesq l("I also do not keep any stock of them. If you really want it, you can bring me some Raw Logs. I'll show you which one is the good one. Deal?");
        next;
        if (BaseLevel < 25) {
            mesn;
            mesq l("Actually, nevermind. I'll wait you grind level 25 first, then we can do this."); // Note: Fisherman part is level 30!
            close;
        }
        mesn strcharinfo(0);
        mesq l("I'm railroaded here, I cannot say no. I'll bring you some @@. Break them until one of them fit.", getitemlink(RawLog));
        setq2 HurnscaldQuest_ForestBow, 1;
        close;
        break;
    case 99:
        mesn;
        mesq l("Good luck...");
        break;
    default:
        goto L_Loop;
    }
    goto L_Close;

L_Loop:
    .@k=getq2(HurnscaldQuest_ForestBow);
    mesn;
    mesq l("Bring me your wood, and I'll show you which one is sturdy, yet flexible enough to make a good Forest Bow.");
    mes "";
    select
        rif(countitem(RawLog) > 0, l("Try this one.")),
        l("Don't break my stuff.");

    if (@menu == 2)
        close;

    mesc l("Jack bends the log over his knee.");
    delitem RawLog, 1;
    .@success=rand(6,26)-.@k; // You will never need more than 20 logs nor less than 5 logs

    if (.@success <= 1) {
        mesc l("The wood bends a little, but doesn't breaks.");
        getexp 295, 100; // 10% of max exp
        setq2 HurnscaldQuest_ForestBow, 99;
        next;
        mesn;
        mesq lg("That's the right wood, my son. Here, I'll wrap it so you don't accidentaly lose it. Good luck with your quest!");
        close;
    } else {
        mesc l("However, the log breaks with a loud crack.");
        Zeny=Zeny+10;
        getexp 5, 1;
        setq2 HurnscaldQuest_ForestBow, .@k+1;
        next;
        mesn;
        mesq lg("Sorry, son. I know how hard this is. But this is teaching you a valuable lesson, your eye will be more sharp for quality wood from now on.");
        next;
    }

    goto L_Loop;

/* Copy-paste from TMW Org.
   I'll uncomment and rewrite when time comes.

L_NohMask_Ask:
    menu
        "I'll keep that in mind.", L_Close,
        "I heard you aren't delivering any more living wood. Why not?", L_Next,
        "Have you seen anything that might be connected to the recent robberies in town?", L_NohMask_Answer;

L_NohMask_Answer:
    mesn "Jack, the Lumberjack";
    mes "\"Sorry, no.\"";
    goto L_Close;

L_Made_Shield:
    mesn "Jack, the Lumberjack";
    mes "\"Speaking of that shield...\"";
    mes "\"It has put me behind on all these new Nivalis orders, now that the ship is running there.\"";
    next;
    mes "\"Care to help out? I'll pay you for your time.\"";
    goto L_Daily;

L_Daily:
    @dq_level = 30;
    @dq_cost = 25;
    @dq_count = 30;
    @dq_name$ = "RawLog";
    @dq_friendly_name$ = "raw logs";
    @dq_money = 4000;
    @dq_exp = 4000;

    callfunc "DailyQuest";

    next;

    mesn "Jack, the Lumberjack";
    mes "\"Check back with me tomorrow, and I'll let you know if I need any help with that shipment.\"";

    goto L_Close;
*/

L_Close:
    close;

OnInit:
    .sex = G_MALE;
    .distance = 5;
    .cost=60;
    end;

}
