// TMW-2 script.
// Author:
//    Crazyfefe
//    Jesusalva
// Description:
//    Barrels
// Variables:
//    CandorQuest_Barrel
// Values:
//    0    Default.
//    1    Quest Found.
//    2    Quest Accepted.
//    3    Bomb Defused.
//    4    Quest Completed.

function	script	CheckBarrel	{
    @barrel_count = 0;
    @count_tmp = 0;
    if ($@GM_OVERRIDE)
        npctalk3 l("Hello, I am B-@@, of the @@ order.", @barrel, $@BarrelBits[@barrel]);
    if (STARTAREA & $@BarrelBits[@barrel])
        goto L_Empty;
    STARTAREA = STARTAREA | $@BarrelBits[@barrel];

    goto L_Loop;

L_BarrelTally:
    areamonster "005-3", 24, 34, 45, 42, "House Maggot", 1084, 1, "Trainer::OnKillHouseMaggot";
    if (@barrel_count == 8)
    {
        message strcharinfo(0), "You found the bug bomb.";
        specialeffect(14);
        specialeffect(51);
        killmonster("005-3", "Trainer::OnKillHouseMaggot");
        setq CandorQuest_Barrel, 3;
    }
    @rand = rand2(5);
    if (@rand == 0)
        getitem Coal,1;
    else if (@rand == 1)
        getitem BugLeg,1;
    else if (@rand == 2)
        getitem CommonCarp,1;
    else if (@rand == 3)
        getitem Croconut,1;
    else if (@rand == 4)
        getitem TolchiArrow,6;
    return;

L_Loop:
    while (@count_tmp < 8) {
        @count_tmp = (@count_tmp + 1);
        if (STARTAREA & $@BarrelBits[@count_tmp])
            @barrel_count = (@barrel_count + 1);
    }
    goto L_BarrelTally;

L_Empty:
    message strcharinfo(0), "You've already searched this barrel.";
    return;

}

005-3,24,36,0	script	Barrel#1	NPC_NO_SPRITE,{
    @barrel = 1;
    .@q = getq(CandorQuest_Barrel);
    if (.@q == 2)
    {
        CheckBarrel();
    }
    end;
OnInit:
    // Remember: array start at zero, but barrels count start at 1. "Fixing" may break barrel 8!
    setarray $@BarrelBits, (1 << 1), (1 << 2), (1 << 3), (1 << 4), (1 << 5), (1 << 6), (1 << 7), (1 << 8), (1 << 9);
    .sex = G_OTHER;
    .distance = 1;
    end;
}

005-3,24,39,0	script	Barrel#2	NPC_NO_SPRITE,{
    @barrel = 2;
    .@q = getq(CandorQuest_Barrel);
    if (.@q == 2)
    {
        CheckBarrel();
    }
    end;
OnInit:
    .sex = G_OTHER;
    .distance = 1;
    end;
}

005-3,29,37,0	script	Barrel#3	NPC_NO_SPRITE,{
    @barrel = 3;
    .@q = getq(CandorQuest_Barrel);
    if (.@q == 2)
    {
        CheckBarrel();
    }
    end;
OnInit:
    .sex = G_OTHER;
    .distance = 1;
    end;
}
005-3,35,34,0	script	Barrel#4	NPC_NO_SPRITE,{
    @barrel = 4;
    .@q = getq(CandorQuest_Barrel);
    if (.@q == 2)
    {
        CheckBarrel();
    }
    end;
OnInit:
    .sex = G_OTHER;
    .distance = 1;
    end;
}
005-3,38,34,0	script	Barrel#5	NPC_NO_SPRITE,{
    @barrel = 5;
    .@q = getq(CandorQuest_Barrel);
    if (.@q == 2)
    {
        CheckBarrel();
    }
    end;
OnInit:
    .sex = G_OTHER;
    .distance = 1;
    end;
}
005-3,44,40,0	script	Barrel#6	NPC_NO_SPRITE,{
    @barrel = 6;
    .@q = getq(CandorQuest_Barrel);
    if (.@q == 2)
    {
        CheckBarrel();
    }
    end;
OnInit:
    .sex = G_OTHER;
    .distance = 1;
    end;
}
005-3,38,41,0	script	Barrel#7	NPC_NO_SPRITE,{
    @barrel = 7;
    .@q = getq(CandorQuest_Barrel);
    if (.@q == 2)
    {
        CheckBarrel();
    }
    end;
OnInit:
    .sex = G_OTHER;
    .distance = 1;
    end;
}
005-3,29,41,0	script	Barrel#8	NPC_NO_SPRITE,{
    @barrel = 8;
    .@q = getq(CandorQuest_Barrel);
    if (.@q == 2)
    {
        CheckBarrel();
    }
    end;
OnInit:
    .sex = G_OTHER;
    .distance = 1;
    end;
}
