// TMW2 scripts.
// Author:
//    Jesusalva
// Description:
//    Tulimshar Inn Recepcionist

003-9,62,128,0	script	Recepcionist#TInn	NPC_LOF_BARKEEPER,{
    // You're on Main Quest - total priority
    if (LUA_ASKED_TO_SLEEP)
        goto L_Cutscene;

    // Otherwise, you cannot sleep here before finishing Lua's Quest
    if (getq(General_Narrator) < 3) {
        npctalk3 l("Hello, and welcome to Tulimshar Inn.");
        emotion E_HAPPY;
        end;
    }

    .@price=limit(400, 700+(BaseLevel*3)-getq(General_Narrator)*6, 1200);
	.@price=limit(250, POL_AdjustPrice(.@price), 1200);

    mesn;
    mesq l("Good %s. Would you like to sleep here? It's only %d GP!", (is_night() ? l("evening") : l("day")), .@price);
    mesc l("Sleeping will fully replenish your health.");
    mesc l("It'll also boost your %s for %s.", b(l("MAX HP")), l("15 minutes."));
    mesc l("Sleeping at night, the bonus will be stronger!");
    next;
    if (askyesno() == ASK_YES && Zeny > .@price) {
        POL_PlayerMoney(.@price);
        percentheal 100, 100;
        .@buff=(is_night() ? 15 : 10);
        SC_Bonus(900, SC_INCMHPRATE, .@buff, .@buff); // FIXME: We don't want stacking
        closeclientdialog;
        warp "003-9", 36, 47;
        dispbottom l("You feel refreshed!");
    }
    close;

L_Cutscene:
    mesn strcharinfo(0);
    mesq l("Hello, Lua told me she booked a room for me.");
    next;
    mesn;
    mesq l("Ah, so you must be %s, the new guy! Yes, it is on the rooms upstairs. I'll bring you there, don't you worry a thing!", strcharinfo(0));
    tutmes l("During cutscenes, you cannot move. If you do, you'll be forcibly pushed back."), l("Cutscenes");
    next;
    closeclientdialog;
	.@mapn$="tinn@"+getcharid(0);
	.@inst = instance_create("Tulim Inn "+getcharid(0), getcharid(3), IOT_CHAR);
    instance_attachmap("003-9", .@inst, false, .@mapn$);
    // Instance lasts 6 minutes
	instance_set_timeout(360, 360, .@inst);
	instance_init(.@inst);
    warp .@mapn$, 36, 47;
    setpcblock(PCBLOCK_HARD, true); // NOTE: No commands?! I'm not sure...
    addtimer 1000, "#TulimInnStory::OnEvent1";
    close;

OnInit:
    .sex = G_FEMALE;
    .distance = 5;
    end;
}

