// TMW2 scripts.
// Authors:
//    Jesusalva
//    Crazyfefe
// Description:
//    A Sailor from Nard's crew.
// CandorQuest_Sailors
//  0: Not started
//  1: Accepted
//  2: Invite Elmo
//  3: Completed

005-1,102,109,0	script	Sailors#005-1	NPC_ELVEN_MAN_TRADER_SITTING,{
    .@q = getq(CandorQuest_Sailors);
    if (.@q == 1)
        goto L_Report;
    if (.@q == 2)
        goto L_Elmo;
    if (.@q == 3)
        goto L_Complete;
    if ( BaseLevel < 8) { hello; end; }

    mesn;
    mesq l("Ahoy matey!");
    next;
    mesq l("Arr, it is always good to be on land after so much time in sea!");
    next;
    mesq l("We want to party this moment, but can you believe we ran out of beer?");
    mes "";
    menu
        l("If I were you, I would drink water."),L_Water,
        l("WHAT? How can you ever do a party without beer?!"),L_Accept;

L_Water:
    mes "";
    mesn;
    mesq l("Ah, it is not the same. Not the same.");
    next;
    mesq l("When I am drunk I tell myself to stop drinking, but I won't listen the words of a drunkard.");
    close;

L_Accept:
    mes "";
    mesn;
    mesq l("We can't, don't you agree?!");
    next;
    mesq l("However, if we waste the ship's money in beer, Nard will get mad.");
    next;
    mesq l("We won't be able to pay you in money, but we'll make you one of us if you bring us beer!");
    next;
    mesq l("Please bring us 5 @@! That should be enough!", getitemlink("Beer"));
    setq CandorQuest_Sailors, 1;
    close;

L_Report:
    mesn;
    mesq l("I see you brought @@/5 @@ for us!",countitem("Beer"),getitemlink("Beer"));
    mes "";
    menu
        rif(countitem("Beer") >= 5, l("Indeed, matey! Here they are!")), L_Give,
        rif(countitem("Beer") >= 5, l("You're doing the math wrong, matey! I'll bring them later!")), L_Later,
        rif(countitem("Beer") < 5, l("Arr, that's not enough! I'll bring more later!")), L_Later;
    close;

L_Later:
    mes "";
    mesn;
    mesq l("Arr, we will wait for you then! We still have tasks to complete!");
    close;

L_Give:
    inventoryplace Bandana, 1;
    delitem "Beer", 5;
    getitem Bandana, 1;
    getexp 35, 5;
    setq CandorQuest_Sailors, 2;
    mes "";
    mesn;
    mesq l("Arr, that's some fine ale! We can do the party when we're done with our work!");
    next;
    mesn;
    mesq l("Take this @@ to prove you're one of us! Could you also invite Elmo? Thanks, matey!", getitemlink("Bandana"));
    close;


L_Elmo:
    mesn;
    mesq l("Please invite Elmo for the party, matey! We can't leave our positions!");
    close;

L_Complete:
    .@q = getq(CandorQuest_SailorCure);
    mesn;
    mesq l("Thanks for the help! Arr, that was some fine ale, indeed!");
    if (.@q == 1)
        close;
    next;
    mesn;
    mesq l("A pity a friend of ours drank too much. Juliet knows how to cure. We need to give her a @@ to do a hangover potion.", getitemlink(ScorpionStinger));

L_CureMaster:
    if (countitem(ScorpionStinger) < 1)
        close;
    next;
    mesn;
    mesq l("...Dealing with scorpion stingers is a gamble, so we may need a few stingers before making a successful potion.");
    next;
    select
        rif(countitem(ScorpionStinger) >= 1, l("I have a Stinger with me. Try it!")),
        l("I see.");

    mes "";

    if (@menu == 1)
        goto L_CureLoop;
    close;

L_CureLoop:
    inventoryplace CandorBoots, 1;
    delitem ScorpionStinger,1;
    setq2 CandorQuest_SailorCure, getq2(CandorQuest_SailorCure)+1;
    if (rand(5) == 2) // Crazyfefe like this number :3
        goto L_questCure_success;
    goto L_questCure_failure;

L_questCure_success:
    if (getq2(CandorQuest_SailorCure) * 30 < 240)
        Zeny = Zeny + 240 - getq2(CandorQuest_SailorCure) * 30;
    else
        Zeny = Zeny + 30;
    getitem CandorBoots, 1;
    getexp 20, 2;
    setq CandorQuest_SailorCure, 1;
    mesn;
    mesq l("That... It... It worked! This is just the right claw!");
    next;
    mesn;
    mesq lg("We'll bring this one to Juliet at once. Thanks for your help! Savior!");
    close;

L_questCure_failure:
    if (getq2(CandorQuest_SailorCure) * 30 < 240)
        Zeny = Zeny + 60;
    else
        Zeny = Zeny + 30;
    mesn;
    mesq l("That... Didn't worked. I'm sorry.");
    next;
    mesn;
    mesq l("Here's some gold for your efforts.");
    goto L_CureMaster;

OnInit:
    .sex = G_OTHER;
    .distance = 7;
    end;
}
