// This file is generated automatically. All manually added changes will be removed when running the Converter.
// Map 015-1: Woodland Mines mobs
015-1,121,105,119,97	monster	Bif	1058,14,45000,45000
015-1,130,81,119,97	monster	Cave Bat	1039,40,60000,60000
015-1,120,136,91,81	monster	Small Amethyst Bif	1110,5,45000,45000
015-1,80,175,142,39	monster	Copper Slime	1088,5,20000,45000
015-1,167,36,11,13	monster	Fire Skull	1193,1,35000,60000
015-1,139,32,57,31	monster	Archant	1026,4,30000,30000
015-1,100,97,26,27	monster	Black Slime	1178,4,40000,30000
015-1,73,111,119,97	monster	Red Slime	1092,75,20000,20000
015-1,147,91,119,97	monster	Yellow Slime	1091,50,30000,20000
015-1,41,136,37,49	monster	Black Scorpion	1074,4,30000,25000
015-1,88,36,142,39	monster	Copper Slime	1088,3,45000,40000
015-1,99,73,3,3	monster	Green Slime	1085,2,45000,60000
015-1,156,144,44,41	monster	Black Scorpion	1074,4,30000,25000
015-1,112,24,39,37	monster	Black Scorpion	1074,4,20000,30000
