// TMW2 Scripts
// Author:
//    Jesusalva
// Description:
//    Controls Forgotten Chamber

/////////////////////////////
018-6-2,90,31,0	script	#FromSouthHall	NPC_SUMMONING_CIRC,0,0,{
    dispbottom l("It looks dangerous.");
    end;

OnTouch:
    .@q=getq(LoFQuest_Barbara);
    .@q2=getq2(LoFQuest_Barbara);
    .@q3=getq3(LoFQuest_Barbara);
    if (.@q == 2) {
        // 1/64 - West Puzzle
        if ((.@q3 & 64)) {
            warp BarbaraInstCheck(0), 68, 90;
            setq3 LoFQuest_Barbara, .@q3|1;
            .@q3=getq3(LoFQuest_Barbara);
            setq3 LoFQuest_Barbara, .@q3-64;
            .@q3=getq3(LoFQuest_Barbara);
            disablenpc instance_npcname(.name$, .@q2);
            // Puzzle complete, enable next NPC
            if (.@q3 == 3)
                enablenpc instance_npcname("#ToSouthHall", .@q2);
            end;
        }
        // 2/128 - East Puzzle
        else if ((.@q3 & 128)) {
            warp BarbaraInstCheck(0), 112, 90;
            setq3 LoFQuest_Barbara, .@q3|2;
            .@q3=getq3(LoFQuest_Barbara);
            setq3 LoFQuest_Barbara, .@q3-128;
            .@q3=getq3(LoFQuest_Barbara);
            disablenpc instance_npcname(.name$, .@q2);
            // Puzzle complete, enable next NPC
            if (.@q3 == 3)
                enablenpc instance_npcname("#ToSouthHall", .@q2);
            end;
        }
        // Wut
        else {
            Exception("ERROR, YOU SHOULD NOT BEEN SEEING THIS. 018-6-2.FSH", RB_DEFAULT|RB_ISFATAL);
        }
        // Active the quest
        if (.@q3 == 3)
            enablenpc instance_npcname("#ToSouthHall", .@q2);
    } else {
        Exception("ERROR, YOU SHOULD NOT BEEN SEEING THIS. 018-6-2.FSH.MQ");
    }
    end;

OnInit:
    disablenpc .name$;
    end;
OnInstanceInit:
    disablenpc instance_npcname(.name$);
    end;

}

/////////////////////////////
018-6-2,70,152,0	script	#FromWestHall	NPC_SUMMONING_CIRC,0,0,{
    dispbottom l("Should I walk on it?");
    end;

OnTouch:
    .@q=getq(LoFQuest_Barbara);
    .@q2=getq2(LoFQuest_Barbara);
    .@q3=getq3(LoFQuest_Barbara);
    if (.@q != 2)
        Exception("ERROR, INVALID WARP", RB_DEFAULT|RB_ISFATAL);

    // East hall not yet enabled, we must repeat
    if (!(.@q3 & 128)) {
        // Mark the west hall as complete (again)
        addtimer(100, "#01862_InstCtrl::OnLevel4");
        setq3 LoFQuest_Barbara, .@q3|64;
        disablenpc instance_npcname(.name$, .@q2);
        warp BarbaraInstCheck(2), 90, 32;
    } else {
        // Quest is over!
        setq3 LoFQuest_Barbara, 7;
        disablenpc instance_npcname(.name$, .@q2);
        enablenpc instance_npcname("#ToForgottenShrine", .@q2);
        warp BarbaraInstCheck(0), 90, 112;
    }
    end;


OnInit:
    disablenpc .name$;
    end;
OnInstanceInit:
    disablenpc instance_npcname(.name$);
    end;
}

/////////////////////////////
018-6-2,103,156,0	script	#FromEastHall	NPC_SUMMONING_CIRC,0,0,{
    dispbottom l("Should I walk on it?");
    end;

OnTouch:
    .@q=getq(LoFQuest_Barbara);
    .@q2=getq2(LoFQuest_Barbara);
    .@q3=getq3(LoFQuest_Barbara);
    if (.@q != 2)
        Exception("ERROR, INVALID WARP", RB_DEFAULT|RB_ISFATAL);

    // West hall not yet enabled, we must repeat
    if (!(.@q3 & 64)) {
        // Mark the east hall as complete (again)
        addtimer(100, "#01862_InstCtrl::OnLevel4");
        setq3 LoFQuest_Barbara, .@q3|128;
        disablenpc instance_npcname(.name$, .@q2);
        warp BarbaraInstCheck(2), 90, 32;
    } else {
        // Quest is over!
        setq3 LoFQuest_Barbara, 7;
        disablenpc instance_npcname(.name$, .@q2);
        enablenpc instance_npcname("#ToForgottenShrine", .@q2);
        warp BarbaraInstCheck(0), 90, 112;
    }
    end;


OnInit:
    disablenpc .name$;
    end;
OnInstanceInit:
    disablenpc instance_npcname(.name$);
    end;
}

