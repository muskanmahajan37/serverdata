// TMW2 scripts.
// Authors:
//    Jesusalva
// Description:
//    Allows movement sliding though the bridge on 014-4

014-4,58,51,0	script	#014-4_58_51	NPC_HIDDEN,0,0,{
    end;
OnTouch:
    slide 51, 51;
    end;
}

014-4,52,51,0	script	#014-4_52_51	NPC_HIDDEN,0,0,{
    end;
OnTouch:
    slide 59, 51;
    end;
}

014-4,25,47,0	script	#014-4_25_47	NPC_HIDDEN,0,0,{
    end;
OnTouch:
    slide 25, 49;
    end;
}

