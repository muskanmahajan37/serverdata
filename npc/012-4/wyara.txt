// TMW2 Script
// Author:
//    Saulc
//    Vasily_Makarov (original from Evol)
//    Jesusalva
// Description:
//    Hurnscald Potion Shopkeeper
// Notes:
//    Reset must be turned in function

012-4,29,28,0	script	Wyara	NPC_FEMALE,{

    speech S_LAST_NEXT,
        l("I am @@, an alchemist specialized in reset potions.", .name$);

L_Menu:
    .@n=getq(General_Narrator);
    .@s=getq(HurnscaldQuest_Sagratha);
    .@s3=getq3(HurnscaldQuest_Sagratha);

    select
        l("Can you reset my stats please?"),
        rif($ARKIM_ST >= 1200,l("I want Piberries Infusion!")),
        rif(getq(HurnscaldQuest_InjuriedMouboo) == 2,l("Do you know how to break curses?")),
        rif(.@s == 1 && .@s3,l("Sagratha was not home.")),
        rif(.@s == 2,l("About Sagratha...")),
        rif(SAGRATHA_SCORE < 0 && .@s >= 6, l("Sagratha hates my guts.")),
        rif(.@n >= 6,l("I am in dire need of Return Potions!")),
        lg("You are weird, I have to go sorry.");

    mes "";
    switch (@menu) {
        case 1:
            goto L_ResetStats;
        case 2:
            goto L_Piberries;
        case 3:
            goto L_Uncurse;
        case 4:
            goto L_SaggyHome;
        case 5:
            goto L_SaggyMain;
        case 6:
            goto L_SaggyHelp;
        case 7:
            goto L_ReturnPot;
        case 8:
            goto L_Quit;
    }

L_ResetStats:
    mesn;
    mesq l("Status point reset can't be undone. Do you really want this?");

L_ConfirmReset:
    ConfirmStatusReset();
    goto L_Quit;

L_Piberries:
    mesn;
    mesq l("For (another) one @@, I'll need 3~4 @@ and 50 GP.", getitemlink(PiberriesInfusion), getitemlink(Piberries));
    next;
    select
        rif(Zeny >= 50 && countitem(Piberries) >= 4, l("Do it!")),
        l("Not now, sorry.");
    if (@menu == 2)
        goto L_Quit;

    mes "";
    inventoryplace PiberriesInfusion, 1;

    Zeny=Zeny-50;
    delitem Piberries, rand2(3,4);
    getitem PiberriesInfusion, 1;
    getexp 10, 0;
    goto L_Piberries;

L_Uncurse:
    mesn;
    mesq l("Well, it depends on the curse. Some are easy to break, and others are... well...");
    next;
    select
        l("It's a simple curse."),
        l("It's a complex curse."),
        l("It's a cursed mouboo.");
    mes "";
    mesn;
    switch (@menu) {
        case 1:
            mesq l("Then you should look in buying Caffeinne. Curse is a status ailment which reduces your attack, nullifies your luck and makes you a snail. Simple Curses can be cured with time, too.");
            break;
        case 2:
            mesq l("Oh, then you should seek the help of an specialist. These curses have an specific condition to break, like leveling up or being killed. Force-breaking them can be difficult.");
            break;
        case 3:
            mesq l("A... Mouboo? Well, I know who can handle curses on cute Mouboos.");
            next;
            mesn;
            mesq l("Go talk to Sagratha, she is usually in a hut in northen forest. The door have a magic barrier, so you'll need to have minimal magic skills to get close enough to open it.");
            next;
            mesn;
            mesq l("She doesn't likes @@s, only cute animals. She doesn't likes Ghosts, Undeads, and Shadow monsters either.", get_race());
            next;
            mesn;
            mesq l("So, when you get on the door, knock it, and say this: \"@@\". She will open the door for you.", b(l("Mouboos are cute")));
            compareandsetq HurnscaldQuest_Sagratha, 0, 1;
            break;
    }
    close;

L_ReturnPot:
    .@price=7000-(reputation("Hurns")*20);
    .@craft=2001-(reputation("Hurns")*20);
    .@ammon=5+(reputation("Hurns")/30);
    mesn;
    mesq l("I understand. Rakinorf told me to stuff you with them if needed.");
    next;
    mesn;
    mesq l("Be aware I can only bake batches of @@ potions.", .@ammon);
    next;
    mesn;
    mesq l("So, it is @@ GP each one up-front. Or I can brew with your materials:", .@price);
    mes "";
    mesn l("Craft Recipe");
    mesc l("- @@/@@ @@", countitem(MauveHerb), 80, getitemlink(MauveHerb));
    mesc l("- @@/@@ @@", countitem(MushroomSpores), 15, getitemlink(MushroomSpores));
    mesc l("- @@/@@ @@", countitem(Potatoz), 7, getitemlink(Potatoz));
    mesc l("- @@/@@ @@", countitem(Coral), 5, getitemlink(Coral));
    mesc l("- @@/@@ @@", countitem(EverburnPowder), 1, getitemlink(EverburnPowder));
    mesc l("- @@/@@ GP", format_number(.@craft), format_number(Zeny));
    next;
    select
        l("Too expensive %%n"),
        rif(Zeny >= .@price, l("I want to pay the full price.")),
        l("I want you to brew some for me.");
    mes "";
    switch (@menu) {
        case 1:
            close;
        case 2:
            mesc l("How many? Max. @@", (Zeny/.@price));
            input .@c, 0, (Zeny/.@price);
            .@payment=.@price*.@c;
            if (Zeny >= .@payment) {
                inventoryplace ReturnPotion, .@c*.@ammon;
                Zeny-=.@payment;
                getitem ReturnPotion, .@c*.@ammon;
                mesn;
                mesq l("There you go!");
            } else {
                Exception("Illegal input.", RB_DEFAULT | RB_SPEECH);
            }
            break;
        case 3:
            mesc l("How many?");
            input .@c, 0, 100;
            inventoryplace ReturnPotion, .@c*.@ammon;
            if (
                countitem(MauveHerb)        <  80*.@c ||
                countitem(MushroomSpores)   <  15*.@c ||
                countitem(Potatoz)          <   7*.@c ||
                countitem(Coral)            <   5*.@c ||
                countitem(EverburnPowder)   <   1*.@c ||
                Zeny < .@craft*.@c
                ) Exception("You don't have that much material.", RB_ISFATAL|RB_SPEECH);
            delitem MauveHerb,       80*.@c;
            delitem MushroomSpores,  15*.@c;
            delitem Potatoz,          7*.@c;
            delitem Coral,            5*.@c;
            delitem EverburnPowder,   1*.@c;
            Zeny-=.@craft*.@c;
            getitem ReturnPotion, .@c*.@ammon;
            mesn;
            mesq l("There you go!");
            break;
    }
    close;

L_SaggyHome:
    mesn;
    mesq l("She probably just went out for a walk. Nothing to worry, I hope.");
    next;
    if (.@n < 11) {
        mesn;
        mesq l("It's impossible to know when she'll be back, so you should prioritize something else for now.");
        close;
    }
    select
        l("Yeah, she might be back soon."),
        l("I don't think so. There have been... Incidents.");
    if (@menu == 1)
        close;
    mes "";
    mesn;
    mesq l("...Incidents?");
    next;
    mesn strcharinfo(0);
    mesc l("You tell her about the incident at the Blue Sage's residence and how they were aiming at sages.");
    next;
    mesn;
    mesq l("Well, this is very disturbing, indeed. I hope my good friend Sagratha is fine.");
    next;
    mesn;
    mesq l("Can you, perhaps, do me a favor? Please... Check her house for any signs of battle.");
    next;
    mesn;
    mesq l("She should be safe as she is a strong woman, but... You never know.");
    setq1 HurnscaldQuest_Sagratha, 2;
    setq3 HurnscaldQuest_Sagratha, 0;
    close;

L_SaggyMain:
    mesn;
    mesq l("Yes? Have you looked her house for signs of battle?");
    next;
    select
        l("Yes, and there was no signs of a fight."),
        l("...Not yet.");
    if (@menu == 2)
        close;
    mes "";
    mesn;
    mesq l("Are you sure? Like, really really sure?");
    next;
    if (!.@s3) {
        mesn strcharinfo(0);
        mesq l("Hmm... Thinking well...");
        next;
        mesn;
        mesq l("THEN DON'T WASTE MY TIME! My friend could be in danger!");
        next;
        mesn;
        mesq l("Go do what I told you to do and examine the house thoroughly!");
        close;
    }
    mesn strcharinfo(0);
    mesq l("Yes, I did. There was only an unlocked secret window.");
    next;
    mesn;
    mesq l("Good, this must mean that Sagratha managed to flee in time.");
    next;
    mesn;
    mesq l("Eh, I don't think you'll be brave enough to go after her. If even she decided to flee, I doubt you wouldn't do the same.");
    next;
    select
        l("Yeah you're right, I'm a noob anyway, and Sagratha is a skilled mage. She should be fine on her own."),
        l("Two still fight better than one. I have the courage of a dustman in me!");
    mes "";
    if (@menu == 1)
        close;
    mesn;
    mesq l("Heh. Foolish. That's what adventurers are, I guess...");
    next;
    mesn;
    mesq l("If you leave by the secret window, you'll notice a small cave entrance. Enter it.");
    next;
    mesn;
    mesq l("It seems to be the ruins of some sort of Mouboo Temple or whatever, from a millenia ago. There may be traps, so be careful.");
    next;
    inventoryplace MercCard_Jesusalva, 1;
    mesn;
    mesq l("I'll give you a @@. It should aid you out there.", getitemlink(MercCard_Jesusalva));
    next;
    mesn;
    mesq l("...Good luck, @@. And be careful. If Sagratha decided to flee... It might be too strong for you.", strcharinfo(0));
    setq1 HurnscaldQuest_Sagratha, 3;
    setq3 HurnscaldQuest_Sagratha, 0;
    getitem MercCard_Jesusalva, 1;
    close;

L_SaggyHelp:
    mesn;
    mesq l("Well, you probably deserve it.");
    next;
    mesn;
    mesq l("Have you been harming the forest? Specially Mouboos. Are you killing them?");
    next;
    mesn;
    mesq l("If yes, of course she will hate you. With reason! You're murdering her family and friends!");
    next;
    mesn;
    mesq l("Listen, she decided to live away from civilization. She choose the forest as her home and the animals as her family.");
    next;
    mesn;
    mesq l("You have no right to take that away from her!");
    next;
    mesn;
    mesq l("You could try to get on her good side by killing what destroys the forest, or by planting trees.");
    next;
    mesn;
    mesq l("But if you keep harming the forest, this will be for naught. Was I clear?");
    close;

L_Quit:
    goodbye;
    close;

OnInit:
    .@npcId = getnpcid(.name$);
    setunitdata(.@npcId, UDT_HEADTOP, FancyHat);
    setunitdata(.@npcId, UDT_HEADMIDDLE, SailorShirt);
    setunitdata(.@npcId, UDT_HEADBOTTOM, BromenalPants);
    setunitdata(.@npcId, UDT_WEAPON, LousyMoccasins); // Boots
    setunitdata(.@npcId, UDT_HAIRSTYLE, 8);
    setunitdata(.@npcId, UDT_HAIRCOLOR, 18);

    .sex = G_FEMALE;
    .distance = 4;
    end;
}
