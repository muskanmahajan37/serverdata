// TMW2 scripts.
// Author:
//    Jesusalva
// Description:
//    Fires of Steam: The Death of Andrei Sakar

//////////////////////////////////////////
// Pentagram Core

029-1,35,32,0	script	Pentagram#FoS_1	NPC_NO_SPRITE,{
    .@n$=strnpcinfo(0, "_0");
    explode(.@ni$, .@n$, "_");
    .@id=atoi(.@ni$[1]);
    debugmes "ID %d", .@id;
    if (.@id < 1) end;
    if ($FIRESOFSTEAM[.@id] < 1) end;

    mesn;
    mesq l("Power remaining: %s", fnum($FIRESOFSTEAM[.@id]));
    next;
    mesc l("Use items to drain the pentagram. Final result is affected by int!");
    mesc l("WARNING: Monsters will appear!"), 1;
    select
        l("Cancel"),
        l("Dark Crystal (5 power)"),
        l("Terranite Ore (3 power)"),
        l("Coal (2 power)"),
        l("Cotton Cloth (1 power)");
    mes "";
    switch (@menu) {
    case 2:
        .@val=5;.@it=DarkCrystal;
        break;
    case 3:
        .@val=3;.@it=TerraniteOre;
        break;
    case 4:
        .@val=2;.@it=Coal;
        break;
    case 5:
        .@val=1;.@it=CottonCloth;
        break;
    default:
        close;
    }

    closeclientdialog;
    if (!countitem(.@it)) end;
    .@val*=countitem(.@it);
    .@val*=(readparam2(bInt)+100)/100; // Each int gives +1%
    delitem .@it, countitem(.@it);
    $FIRESOFSTEAM[.@id]-=.@val;
    getexp 0, .@val*2;
    // TODO: Check & unlock
    if ($FIRESOFSTEAM[.@id] < 1)
        disablenpc .name$;
    // Create monsters based on effectivity
    areamonster("029-1", .x-3, .y-3, .x+3, .y+3, strmobinfo(1, MagicGoblin), MagicGoblin, (.@val/15)+1, "SteamFire#Ctrl::OnEvent1");
    dispbottom l("Power remaining: %s", fnum($FIRESOFSTEAM[.@id]));
    close;

OnInit:
    .distance=3;
    end;
}

// Now we duplicate
029-1,258,39,0	duplicate(Pentagram#FoS_1)	Pentagram#FoS_2	NPC_NO_SPRITE
029-1,113,148,0	duplicate(Pentagram#FoS_1)	Pentagram#FoS_3	NPC_NO_SPRITE
029-1,229,188,0	duplicate(Pentagram#FoS_1)	Pentagram#FoS_4	NPC_NO_SPRITE
029-1,38,257,0	duplicate(Pentagram#FoS_1)	Pentagram#FoS_5	NPC_NO_SPRITE



