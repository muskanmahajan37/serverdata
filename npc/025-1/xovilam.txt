// TMW2 Script
// Author:
//    Jesusalva

025-1,143,59,0	script	Xovilam	NPC_PLAYER,{

    speech S_LAST_NEXT,
        l("I am @@, an alchemist specialized in reset potions.", .name$);

    select
        l("Can you reset my stats please?"),
        l("You are weird, I have to go sorry.");

    switch (@menu)
    {
        case 1:
            goto L_ResetStats;
        case 2:
            goto L_Quit;
    }

L_ResetStats:
    mesn;
    mesq l("Status point reset can't be undone. Do you really want this?");

L_ConfirmReset:
    ConfirmStatusReset(1000, false);
    goto L_Quit;


L_Quit:
    goodbye;
    end;

OnInit:
    .@npcId = getnpcid(.name$);
    setunitdata(.@npcId, UDT_HEADTOP, FancyHat);
    setunitdata(.@npcId, UDT_HEADMIDDLE, SailorShirt);
    setunitdata(.@npcId, UDT_HEADBOTTOM, BromenalPants);
    setunitdata(.@npcId, UDT_WEAPON, LousyMoccasins); // Boots
    setunitdata(.@npcId, UDT_HAIRSTYLE, 7);
    setunitdata(.@npcId, UDT_HAIRCOLOR, 17);

    .sex = G_MALE;
    .distance = 4;
    end;
}
