// TMW2 Script
// Authors:
//    Jesusalva
// Description:
//    Link portals to soul menhirs like the teleporters from old
//    The price is temporary. This feature got in because no ship in Nivalis Port
//    PS. Anise => “Aisen” Anagram


025-1,40,115,0	script	#WarpGateFort	NPC_HIDDEN,1,0,{
    end;

OnTouch:
    TeleporterGate(TP_FORT);
    close;


OnInit:
    .sex = G_OTHER;
    .distance = 1;
    end;
}

