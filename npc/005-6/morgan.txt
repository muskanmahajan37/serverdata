// TMW2 Script
// Author:
//    Jesusalva
// Description:
//  Morgan is the only mage in Candor. She was assigned by the Alliance to defend
//  the small island, and is married with Zitoni.

005-6,30,37,0	script	Morgan	NPC_RUMLY_FEMALE,{

    mesn;
    mesq lg("Hello there, darling.");
    next;
    menu
        l("Who are you?"), L_Who,
        l("Do you sell anything here?"), L_Shop,
        l("Hi."), -; // Let's be honest, many conversations dies this way >.<
    close;

L_Who:
    mes "";
    mesn;
    mesq l("I am @@, the only mage in Candor.", .name$);
    next;
    mesq l("My husband and I have moved here under the Alliance orders.");
    next;
    mesq l("I really hope that nothing major happens, though. I would hate to see blood being spilled.");
    next;
    if (!MAGIC_LVL)
        mesq l("If you train hard enough, and get access to a Mana Stone, I could teach you a trick or two. But right now, I don't sense magic compatibility in you.");
    else
        goto L_Magic;
    close;

L_Shop:
    mes "";
    mesn;
    mesq l("Why, I actually do sell a few things here! But they probably aren't what you're looking for.");
    next;
    openshop;
    closedialog;
    close;

// Magic Subsystem
L_Magic:
    mesn;
    mesq l("I see you have magical powers. Have you been acknowledge by the Alliance as a mage? If not, you should regularize yourself as a @@º degree mage in Tulimshar Magic Council.", MAGIC_LVL);

L_MagicCore:
    select
        l("Acknowledgment? What do you mean?"),
        l("Magic Council? What's that?"),
        rif(!getskilllv(TMW2_KALMURK), l("Can you teach me a spell?")),
        rif(getskilllv(TMW2_KALMURK), l("I keep failing to summon maggots...")),
        l("Thanks for the help!");
    mes "";
    switch (@menu) {
    case 1:
        mesn;
        mesq l("You won't learn any skill without a good professor to teach them to you.");
        next;
        mesn;
        mesq l("And unless you have a mage permit from the Magic Council, you won't be going to CR1 - Academy Island either.");
        break;
    case 2:
        mesn;
        mesq l("You know the Tulimshar Magic Council? The big building on Tulimshar North?");
        next;
        mesn;
        mesq l("It rules Tulimshar, and is also one of the superior bodies of the Alliance. They have the last Mana Stone in the world on their meeting hall.");
        next;
        mesn;
        mesq l("They restrict access to it to prevent new Monster Kings from emerging. They also keep record on every mage in the world.");
        next;
        mesn;
        mesq l("In general, by touching the Mana Stone once, will make you a first degree mage. But these degrees goes up to seven and beyond, I think.");
        next;
        mesn;
        mesq l("Of course, if you don't ask from the Mana Stone for more magic, you will stay on the first degree forever...");
        break;
    case 3:
        mesn;
        mesq l("Sure! But that is Summon Magic. Just bring me a mug of beer, I'm thristy. And I'll teach you a basic skill.");
        next;
        if (askyesno() == ASK_YES) {
            if (!countitem(Beer)) goto L_Cheat;
            delitem Beer, 1;
            skill(TMW2_KALMURK,1,0);
            mesn;
            mesq l("You can use @@ to summon some maggots. That depends on your magic level, of course.", b("Kalmurk"));
            next;
            mesn;
            mesq l("That's a Summon Magic. Unlike regular magic, you can increase its levels directly from skill menu (F5).");
            next;
            mesn;
            mesq l("Keep in mind the more power you have, the less control you'll have as well. Until you get a %s to track progress, use %s to check your control. Less control may result in failure to summon!", getitemlink(JesusalvaGrimorium), b("@abizit"));
            next;
            mesn;
            mesq l("By the way, you need a couple of @@ to try the skill. Fail rate is pretty high if you don't know how to control your magic.", getitemlink(MaggotSlime));
        }
        break;
    case 4:
        mesn;
        mesq l("Summoning is part of Mana Magic. Mana Magic is trickier, you must have control over magic power to use it properly.");
        next;
        mesn;
        mesq l("This means everytime you get more power, you lose control. Pratice is the key.");
        next;
        mesn;
        mesq l("Try praticing with different mana magic skills. Anyway, you can use this command to see your magic proeficiency: " + b("@abizit"));
        break;
    default:
        close;
    }
    next;
    goto L_MagicCore;


L_Cheat:
    mesn;
    mesq l("I offer an awesome skill for you, for a very slow price, and you try to cheat me... pitiful...");
    close;

OnInit:
	tradertype(NST_MARKET);

    sellitem CottonCloth, -1, 7;
    sellitem Fungus, -1, 10;
    sellitem Bread, -1, 15;

    .sex = G_FEMALE;
    .distance = 5;
    end;

OnClock0556:
OnClock1201:
OnClock1759:
OnClock0003:
    restoreshopitem CottonCloth, 7;
    restoreshopitem Fungus, 10;
    restoreshopitem Bread, 15;
}
