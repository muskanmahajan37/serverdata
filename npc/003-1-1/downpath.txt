// TMW2 scripts.
// Author:
//    Jesusalva
// Notes:
//    TW: Tulimshar, West
//    TE: Tulimshar, East

003-1-1,100,58,0	script	#SewerDD-TW	NPC_NO_SPRITE,{
    mesc l("There's a small, damp corritor, which you could crawl though.");
    mesc l("It's not possible to see any light, and it seems to small and damp to have monsters.");
    mesc l("You could barely fit on it, and your clothes will be ruined, in need of washing.");
    next;
    mesc l("Descend into the small corritor?");
    mesc l("Note: You'll be vulnerable for a short while!"), 1;
    if (askyesno() == ASK_YES) {
        closedialog;
        sc_start SC_STUN, 3000, 1, 10000, SCFLAG_NOAVOID|SCFLAG_FIXEDTICK, getcharid(3);
        sleep2(3000);
        if (ispcdead()) end;
        warp "003-1-3", 45, 36;
        dispbottom l("At long last, you see the end of the corritor.");
        RegEasterEgg(EE_SEWERS, 3);
        end;
    }
    close;
}

003-1-1,150,140,0	script	#SewerDD-TE	NPC_NO_SPRITE,{
    dispbottom l("The sewer mouth is locked.");
    end;
}

