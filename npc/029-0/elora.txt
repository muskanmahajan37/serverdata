// TMW2 scripts.
// Author:
//    Jesusalva
// Description:
//    Fires of Steam: The Death of Andrei Sakar

029-0,182,77,0	script	Elora	NPC_HUMAN_FEMALE_NOOB,{
    mesn;
    mesq l("Greetings! I am %s from the Alliance.", .name$);
    next;
    switch ($FIRESOFSTEAM) {
        case 1:
            mesn;
            mesq l("Thanks for travelling with Andrei Sakar to Artis! However, as you see, this town is deserted.");
            next;
            mesn;
            mesq l("Sir Sakar is conducting an exploration on this land. However, you should be able to refine and tweak equipment, as well as crafting your own. Once you're ready, find him at %s of this town.", b(l("the north exit")));
            break;
        case 2:
        case 3:
        case 4:
        case 5:
        case 6:
            mesn;
            mesq l("Sir Sakar and adventurers are raiding Dracula's Fortress. It is a crazy place, though, who knows what they'll find? He should be at the north exit of this town.");
            break;
        case 7:
            mesn;
            mesq l("Sir Sakar and adventurers are raiding Dracula's Fortress. They found out the Underworld and there is a magic tree. You'll need a %s to use it.", getitemlink(DruidTreeBranch));
            mesc l("They are at north of the town.");
            next;
            mesn;
            if (FIRESOFSTEAM_CD < gettimetick(2)) {
                mesq l("Here is one.");
                FIRESOFSTEAM_CD=gettimetick(2)+86400; // 24 hours
                getitem DruidTreeBranch, 1;
            } else {
                mesq l("Some other adventurers are looking for it. Please wait %s more.", FuzzyTime(FIRESOFSTEAM_CD));
            }
            break;
        case 8:
            mesn;
            mesq l("Sakar can now bring you directly to Asphodel Moors. Scouts found a second town as well, from where the raiders seems to be coming from!");
            break;
        case 9:
            mesn;
            mesq l("Contact a GM at Migglemire - the small town at center of swamps - to trigger the final showdown.");
            break;
    }
    close;

OnInit:
    .distance=5;
    .sex=G_FEMALE;
    if ($FIRESOFSTEAM >= 10)
        disablenpc .name$;
    end;
}

// UTILITY
029-0,96,110,0	duplicate(Nicholas)	Nicholas#FoS	NPC_NICHOLAS
029-0,89,75,0	duplicate(Fortiun)	Fortiun#FoS	NPC_FORTIUN
029-0,68,87,0	duplicate(Tolchi)	Tolchi#FoS	NPC_RAIJIN_FEMALE_LEGION_ARTIS
029-0,137,75,0	duplicate(Bracco)	Bracco#FoS	NPC_ORC_SAILOR
029-0,90,130,0	duplicate(Zitoni)	Zitoni#FoS	NPC_RUMLY
029-0,132,51,0	duplicate(Cynric)	Cynric#FoS	NPC_LLOYD
029-0,57,55,0	duplicate(Mercenary Trainer)	Mercenary Trainer#FoS	NPC_REDY_MALE_SWORD
029-0,123,84,0	duplicate(Estard)	Estard#FoS	NPC_KENTON
//029-0,143,120,0	duplicate(Guild Storage)	Guild Storekeeper#FoS	NPC_TERRY
029-0,198,34,0	duplicate(Colin)	Colin#FoS	NPC_MIRAJ
029-0,182,26,0	duplicate(Luca)	Luca#FoS	NPC_ANSELMO_BR
029-0,200,49,0	duplicate(Mr Saves)	Mr Saves#FoS	NPC_BLACKALCHEMIST
029-0,179,38,0	duplicate(Trickmaster)	Trickmaster#FoS	NPC_SITTED_NINJA
029-0,142,33,0	duplicate(Crafting Table)	Crafting Box#FoS	NPC_NO_SPRITE
029-0,141,51,0	duplicate(Intense Beard)	Intense Beard#FoS	NPC_BRGUARD_SPEAR
029-0,127,32,0	duplicate(Jhedia)	Jhedia#FoS	NPC_ELVEN_FEMALE
029-0,58,118,0	duplicate(Neko)	Neko#FoS1	NPC_KAYLO

// 029-1
029-1,35,88,0	duplicate(Neko)	Neko#FoS2	NPC_KAYLO
029-1,29,88,0	script	Elora#1	NPC_HUMAN_FEMALE_NOOB,{
    mesn;
    mesq l("Good luck out there!");
    .@m=mobcount("029-1", "all");
    mesc l("Dark Forest : %s monster(s) left", fnum(.@m));
    next;
    select
        l("Thanks!"),
        l("Actually, can I return to Artis?");
    mes "";
    closeclientdialog;
    if (@menu == 2)
        warp "029-0", 90, 31;
    close;
OnInit:
    .distance=5;
    .sex=G_FEMALE;
    end;
}

029-8,37,170,0	duplicate(Neko)	Neko#FoS3	NPC_KAYLO


