// TMW2 scripts.
// Authors:
//    Jesusalva
// Description:
//    Exclusive for Fires of Steam 2021 - When our OVH server datacenter got fire.

029-0,111,37,0	script	Alliance Hero#FoS	NPC_HALBERDBARBARIAN,{
    // No one is supposed to be able to reach NPC if event did not happen
    if (!$FIRESOFSTEAM) {
        disablenpc .name$;
        die(); end;
    }
    mesn;
    mesq l("Sooo, the police station of Moubootaur Legends went ablaze. Constable Perry is too busy to monitor illegal operation on this continent.");
    next;
    mesn;
    mesq l("Therefore... Are you perhaps interested in some... illicit goods? %%p");
    next;
    select
        l("Show me what you got, noob."),
        l("Actually - Do you exchange goods even more illegal goods than this?"),
        l("Actually - Do you exchange illegal... research results?");
    mes "";
    if (@menu == 1) {
        closeclientdialog;
        openshop .name$;
        close;
    }
    else if (@menu == 2) {
        mesn;
        mesq l("Yes I do... I sell %s %s for the small amount of %s GP. Interested? %%%%p", fnum(.mobp), l("Monster Points"), fnum(.mobg));
        if (Zeny < .mobg) close;
        next;
        if (askyesno() == ASK_NO) close;
        if (Zeny < .mobg) { die(); close; }
        Zeny-=.mobg;
        Mobpt+=.mobp;
        mes "";
        mesn;
        mesq l("Hehehe... A pleasure doing business with you!");
    } else if (@menu == 3) {
        mesn;
        mesq l("Yes I do... I sell %s %s for the small amount of %s GP. Interested? %%%%p", fnum(.robp), l("Research Points"), fnum(.robg));
        if (Zeny < .robg) close;
        next;
        if (askyesno() == ASK_NO) close;
        if (Zeny < .robg) { die(); close; }
        Zeny-=.robg;
        Mobpt+=.robp;
        mes "";
        mesn;
        mesq l("Hehehe... A pleasure doing business with you!");
    }
    close;

OnRw:
    logmes(sprintf("%s - Reward Granted by System Admin", getcharid(3)));
    getitem StrangeCoin, 2000;
    getitembound FireScroll, 1, 1;
    getexp 1000000, 500000;
    Mobpt+=1000000;
    Zeny+=1000000;
    dispbottom l("Jesusalva : \\o/");
    end;

OnInit:
    bindatcmd "steambk", "Alliance Hero#FoS::OnRw", 100, 99, 1;
	tradertype(NST_MARKET);
    .mobp=rand2(1000, 2500);
    .mobg=.mobp*rand2(100, 150)/10;
    .robp=rand2(900, 1200);
    .robg=.robp*rand2(40, 60);//rand2(80, 110);
    .distance=5;
    .sex=G_MALE;

    sellitem Coal,          -1, 50+($FIRESOFSTEAM*3-3);
    sellitem LeatherPatch,  800, 45+($FIRESOFSTEAM*3-3);
    sellitem RawLog,        -1, 40+($FIRESOFSTEAM*3-3);
    sellitem WoodenLog,     -1, 40+($FIRESOFSTEAM*3-3);
    sellitem IronOre,       -1, 30+($FIRESOFSTEAM*3-3);
    sellitem CopperOre,     1000, 20+($FIRESOFSTEAM*2-2);
    sellitem SilverOre,     2000, 14+($FIRESOFSTEAM*2-2);
    sellitem GoldOre,       3000, 14+($FIRESOFSTEAM*2-2);
    sellitem TinOre,        3600, 15+($FIRESOFSTEAM*2-2);
    sellitem LeadOre,       4000, 15+($FIRESOFSTEAM*2-2);
    sellitem TitaniumOre,   6000, 9+($FIRESOFSTEAM*2-2);
    sellitem IridiumOre,    16000, 6+($FIRESOFSTEAM*2-2);
    sellitem PlatinumOre,   24000, 3+($FIRESOFSTEAM*2-2);
    sellitem EarthPowder,   -1, 3+$FIRESOFSTEAM-1;
    sellitem EverburnPowder, 15000, 2+$FIRESOFSTEAM-1;

    sellitem AncientBlueprint,  12500, max(1, 4+$FIRESOFSTEAM/2);
    sellitem RustyKnife,        -1, 5+$FIRESOFSTEAM-1;
    sellitem TrainingWand,      -1, 5+$FIRESOFSTEAM-1;
    sellitem TrainingBow,       -1, 5+$FIRESOFSTEAM-1;

    sellitem AlchemyBlueprintA, -1, 5+$FIRESOFSTEAM-1;
    sellitem AlchemyBlueprintB, -1, 4+$FIRESOFSTEAM-1;
    sellitem AlchemyBlueprintC, -1, 3+$FIRESOFSTEAM-1;
    sellitem AlchemyBlueprintD, -1, 2+$FIRESOFSTEAM-1;
    sellitem AlchemyBlueprintE, -1, max(1, 1+$FIRESOFSTEAM/2);

    sellitem EquipmentBlueprintA, -1, 5+$FIRESOFSTEAM-1;
    sellitem EquipmentBlueprintB, -1, 4+$FIRESOFSTEAM-1;
    sellitem EquipmentBlueprintC, -1, 3+$FIRESOFSTEAM-1;
    sellitem EquipmentBlueprintD, -1, 2+$FIRESOFSTEAM-1;
    sellitem EquipmentBlueprintE, -1, max(1, 1+$FIRESOFSTEAM/2);

    sellitem ArcmageBoxset,     10000, 4+$FIRESOFSTEAM-1;
    sellitem ScholarshipBadge,  -1, 3+$FIRESOFSTEAM-1;
    sellitem Bullet,            4, 90000+($FIRESOFSTEAM*1000-1000);
    sellitem Lifestone,         -1, 500+$FIRESOFSTEAM-1;
    sellitem Bread,             -1, 450+$FIRESOFSTEAM-1;
    sellitem Cheese,            -1, 300+$FIRESOFSTEAM-1;
    sellitem Aquada,            -1, 200+$FIRESOFSTEAM-1;

    sellitem WhiteFur,          -1, 1+rand2(50)+$FIRESOFSTEAM;
    sellitem Piberries,         -1, 1+rand2(50)+$FIRESOFSTEAM;
    sellitem CherryCake,        -1, 1+rand2(50)+$FIRESOFSTEAM;
    sellitem LettuceLeaf,       -1, 1+rand2(50)+$FIRESOFSTEAM;
    sellitem BugLeg,            -1, 1+rand2(50)+$FIRESOFSTEAM;
    sellitem RoastedMaggot,     -1, 1+rand2(50)+$FIRESOFSTEAM;
    sellitem Moss,              -1, 1+rand2(50)+$FIRESOFSTEAM;
    sellitem AnimalBones,       -1, 1+rand2(40)+$FIRESOFSTEAM;
    sellitem Milk,              -1, 1+rand2(50)+$FIRESOFSTEAM;
    sellitem Mashmallow,        -1, 1+rand2(50)+$FIRESOFSTEAM;
    sellitem Dragonfruit,       1200, 1+rand2(50)+$FIRESOFSTEAM;
    sellitem Root,              -1, 40+$FIRESOFSTEAM-1;
    sellitem ManaPiouFeathers,  -1, 1+rand2(300)+$FIRESOFSTEAM;

    if (!$BETASERVER && !debug)
        disablenpc .name$;

    if ($BETASERVER &&!$@FOS_MOBDROP) {
        /* This code is overriden by seasons.txt
        addmonsterdrop(Scar, DarkCrystal, 500);
        addmonsterdrop(Crafty, DarkCrystal, 500);
        addmonsterdrop(GiantMutatedBat, DarkCrystal, 550);
        addmonsterdrop(Forain, DarkCrystal, 500);
        addmonsterdrop(GreenDragon, DarkCrystal, 500);
        addmonsterdrop(EliteDuck, DarkCrystal, 550);
        addmonsterdrop(Terranite, DarkCrystal, 500);
        addmonsterdrop(JackO, DarkCrystal, 550);
        addmonsterdrop(RedSkullSlime, DarkCrystal, 500);
        addmonsterdrop(Michel, DarkCrystal, 500);
        addmonsterdrop(CopperSkullSlime, DarkCrystal, 500);
        addmonsterdrop(MonsterGeneral, DarkCrystal, 10000);
        */
        setiteminfo(DarkCrystal, ITEMINFO_SELLPRICE, rand2(150, 250));
        $@FOS_MOBDROP=true;
    }
    end;

OnClock0001:
OnClock0201:
OnClock0401:
OnClock0601:
OnClock0801:
OnClock1001:
OnClock1201:
OnClock1401:
OnClock1601:
OnClock1801:
OnClock2001:
    if ($EVENT$ != "Steam")
        end;
OnClock2201:
    .mobp=rand2(1000, 2500);
    .mobg=.mobp*rand2(100, 150)/10;
    .robp=rand2(900, 1200);
    .robg=.robp*rand2(40, 60);//rand2(80, 110);

    restoreshopitem Coal,           50+($FIRESOFSTEAM*3-3);
    restoreshopitem LeatherPatch, 800, 45+($FIRESOFSTEAM*3-3);
    restoreshopitem RawLog,         40+($FIRESOFSTEAM*3-3);
    restoreshopitem WoodenLog,      40+($FIRESOFSTEAM*3-3);
    restoreshopitem IronOre,        30+($FIRESOFSTEAM*3-3);
    restoreshopitem CopperOre,   1000, 20+($FIRESOFSTEAM*2-2);
    restoreshopitem SilverOre,   2000, 14+($FIRESOFSTEAM*2-2);
    restoreshopitem GoldOre,     3000, 14+($FIRESOFSTEAM*2-2);
    restoreshopitem TinOre,      3600, 15+($FIRESOFSTEAM*2-2);
    restoreshopitem LeadOre,     4000, 15+($FIRESOFSTEAM*2-2);
    restoreshopitem TitaniumOre, 6000, 9+($FIRESOFSTEAM*2-2);
    restoreshopitem IridiumOre,  16000, 6+($FIRESOFSTEAM*2-2);
    restoreshopitem PlatinumOre, 24000, 3+($FIRESOFSTEAM*2-2);
    restoreshopitem EarthPowder, 3+$FIRESOFSTEAM-1;
    restoreshopitem EverburnPowder, 15000, 2+$FIRESOFSTEAM-1;

    restoreshopitem AncientBlueprint, 10000, 4+$FIRESOFSTEAM-1;
    restoreshopitem RustyKnife, 5+$FIRESOFSTEAM-1;
    restoreshopitem TrainingWand, 5+$FIRESOFSTEAM-1;
    restoreshopitem TrainingBow, 5+$FIRESOFSTEAM-1;

    restoreshopitem AlchemyBlueprintA, 5+$FIRESOFSTEAM-1;
    restoreshopitem AlchemyBlueprintB, 4+$FIRESOFSTEAM-1;
    restoreshopitem AlchemyBlueprintC, 3+$FIRESOFSTEAM-1;
    restoreshopitem AlchemyBlueprintD, 2+$FIRESOFSTEAM-1;
    restoreshopitem AlchemyBlueprintE, 1+$FIRESOFSTEAM-1;

    restoreshopitem EquipmentBlueprintA, 5+$FIRESOFSTEAM-1;
    restoreshopitem EquipmentBlueprintB, 4+$FIRESOFSTEAM-1;
    restoreshopitem EquipmentBlueprintC, 3+$FIRESOFSTEAM-1;
    restoreshopitem EquipmentBlueprintD, 2+$FIRESOFSTEAM-1;
    restoreshopitem EquipmentBlueprintE, 1+$FIRESOFSTEAM-1;

    restoreshopitem ArcmageBoxset, 10000, 4+$FIRESOFSTEAM-1;
    restoreshopitem ScholarshipBadge, 3+$FIRESOFSTEAM-1;
    restoreshopitem Lifestone, 800+$FIRESOFSTEAM-1;
    restoreshopitem Bullet, 4, 90000+$FIRESOFSTEAM-1;
    restoreshopitem Bread,  750+$FIRESOFSTEAM-1;
    restoreshopitem Cheese, 400+$FIRESOFSTEAM-1;
    restoreshopitem Aquada, 200+$FIRESOFSTEAM-1;

    restoreshopitem WhiteFur, 1+rand2(50)+$FIRESOFSTEAM;
    restoreshopitem Piberries, 1+rand2(50)+$FIRESOFSTEAM;
    restoreshopitem CherryCake, 1+rand2(50)+$FIRESOFSTEAM;
    restoreshopitem LettuceLeaf, 1+rand2(50)+$FIRESOFSTEAM;
    restoreshopitem BugLeg, 1+rand2(50)+$FIRESOFSTEAM;
    restoreshopitem RoastedMaggot, 1+rand2(50)+$FIRESOFSTEAM;
    restoreshopitem Moss, 1+rand2(50)+$FIRESOFSTEAM;
    restoreshopitem AnimalBones, 1+rand2(40)+$FIRESOFSTEAM;
    restoreshopitem Milk, 1+rand2(50)+$FIRESOFSTEAM;
    restoreshopitem Mashmallow, 1+rand2(50)+$FIRESOFSTEAM;
    restoreshopitem Dragonfruit, 1200, 1+rand2(50)+$FIRESOFSTEAM;
    restoreshopitem Root, 40+$FIRESOFSTEAM;
    restoreshopitem ManaPiouFeathers, 1+rand2(300)+$FIRESOFSTEAM;
    end;

OnMinute02:
    if ($EVENT$ != "Steam") end;
    donpcevent "Neko::OnClock2359";
    end;
}


003-0-2,34,21,0	script	Alliance Officer	NPC_HALBERDBARBARIAN,{
    mesn;
    mesq l("Good %s, %s. The council is not in session right now.",
          (is_night() ? l("evening") : l("morning")),
          (strcharinfo(0) == $MOST_HEROIC$ ? lg("hero") : lg("peasant")));
    if (($BETASERVER || debug) && BaseLevel < 60 && !#BETA_REVIVE) goto L_PowerUp;
    close;

L_PowerUp:
    next;
    mesn strcharinfo(0);
    mesq l("Could you awake my lost and forsaken potential?");
    next;
    mesn;
    mesq l("Yes. I can. But this is irreversible. Are you sure you want this?");
    next;
    mesc l("Awake lost potential? This will mess with your char data irreversibly, beware."), 1;
    if (askyesno() == ASK_NO) close;
    inventoryplace Iten, 1, NPCEyes, 4;

    // Chose a stage (NO TTL)
    mesc l("Please select where you left off on Main Quest.");
    mesc l("The one with a star (*) is advised.");
    mesc l("It is NOT advised for new players to skip parts of the Main Quest.");
    mesc l("Skipping will FORSAKE rewards for the quest and related; So choose wisely!");
    mes "";
    menuint
        "Nard Quest finished", 1,
        "Lua Quest finished", 3,
        "Airlia Quest finished", 6,
        "Librarian Quest finished", 10,
        "Blue Sage Quest finished", 12,
        "(*) King Gelid Quest complete", 17,
        "Lightbringer/Barbara finished", 19;
    mes "";
    // Save stage
    .@mq = @menuret;

    // IP Blacklist
    if (array_find($@IPBLIST$, getcharip()) < 0)
        array_push($@IPBLIST$, getcharip());

    // Level up
    freeloop(true);
    while (BaseLevel < 60)
        getexp NextBaseExp, 100;
    freeloop(false);

    // Skip a few quests
    setq ShipQuests_Arpan, 5;
    if (.@mq > 10)
        setq NivalisQuest_BlueSage, 12;
    if (.@mq >= 17) {
        sk_lvup(AM_REST);
        sk_lvup(AM_RESURRECTHOMUN);
        sk_lvup(AM_CALLHOMUN);
    }
    if (.@mq >= 18) {
        BARBARA_STATE=any(1, 2, 3);
    }

    // Update main quest
    if (getq(General_Narrator) < .@mq)
        setq General_Narrator, .@mq;

    // Monster points
    MPQUEST=true;
    if (!Mobpt)
        Mobpt+=100000;

    // Magic Power
    adddefaultskills();
    if (!MAGIC_LVL) {
        sk_lvup(AL_DP);
        MAGIC_LVL=1;
    }

    // Crafting
    if (!CRAFTQUEST) {
        sk_lvup(TMW2_CRAFT);
        getitembound RecipeBook, 1, 4;
        CRAFTQUEST=true;
    }

    // Free skills
    sk_lvup(TMW2_MANABOMB);
    sk_lvup(any(TMW2_FROSTDIVER, TMW2_NAPALMBEAT, TMW2_MAGICSTRIKE, TMW2_METEORSTRIKE, TMW2_FIREARROW, TMW2_BRAWLING, TMW2_FALKONSTRIKE, TMW2_CHARGEDARROW));

    // Pure awesomeness
    getitembound any(StrengthFruit, AgilityFruit, VitalityFruit, IntelligenceFruit, DexterityFruit, LuckFruit), 1, 4;
    getitembound any(StrengthFruit, AgilityFruit, VitalityFruit, IntelligenceFruit, DexterityFruit, LuckFruit), 1, 4;

    // Full power
    getitembound Wurtzite, 6, 4;
    getitembound Bread, 10, 4;

    // Regeneration and misc
    getitem StrangeCoin, 100;
    Zeny+=50000;
    percentheal 100,100;
    mesc l("You awake a long forgotten potential, and feel ready to take over the world.");
    #BETA_REVIVE=true;
    close;

OnInit:
    .distance=4;
    /*
    if (!$BETASERVER && !debug)
        disablenpc .name$;
    */
    end;
}

